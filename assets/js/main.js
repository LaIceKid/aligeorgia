$(document).ready(function(){
  $('[data-toggle="popover"]').popover();
});

message_fade_out();
$('.carousel').carousel();

//ONLOAD FUNTIONS
// ajaxProcess($("#login-container"),"login-container");



function ajaxProcess(element, container){
  $("#"+container).html('');
  $("#"+container).append('<div id="preloader-'+container+'" class="lds-ring"><div></div><div></div><div></div><div></div><div></div></div>');

  let url = $(element).data('href');

  var request = $.post(url, {key: "success"}, function() {

  });

  request.done(function(response) {
    $("#"+container).html(response);
  });

  request.fail(function(jqXHR, textStatus) {
    alert("Request failed: " + textStatus);
  });
}

function message_fade_out(){
  if ($(".message-display").length > 0){
    setTimeout(function(){
      $(".message-display").fadeOut(300);//FADE OUT MESSAGE

      setTimeout(function(){
        $(".message-display").remove();//REMOVE MESSAGE
      }, 500);

    }, 10000);
  }
}

function readURL(input) {
  let productid = $(input).data("productid");
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      //$('#selected-image-container').attr('src', e.target.result);
      $('#selected-image-container-'+productid).css('background-image', 'url(' + e.target.result + ')');
    };
    reader.readAsDataURL(input.files[0]);
  }
}

let masonryPackage = () => {
    $('.masonry-container').masonry({
      itemSelector: '.package'
    });

};

let getData = (element, start, amount) => {
  let paneldata = $('#'+element);
  let rooturl = paneldata.data('rooturl');
  let role = paneldata.data('role');
  if(role === 'awaiting'){
    role = '';
  }
  let packagedata = 'packagedata';
  if(role === 'sent' || role === 'giveaway' || role === 'arrived' || role === 'obtained'){
    packagedata = 'packagedataflight';
  }

  $.ajax({
    type: 'GET',
    url: ''+rooturl+packagedata+'/?status='+role+'&start='+start+'&amount='+amount,
    data: {},
    cache: false,
    beforeSend(){
      paneldata.html('<div class="preloader-cog"><i class="fa fa-cog fa-spin fa-4x text-danger"></i></div>');
    },
    success: function(data){
      paneldata.html(data);
      masonryPackage();
    }
  });
};
getData("data-container", 0, 100);

// disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

let getDeclarForm = (element) => {
  let trackingCode = $(element).data('tracking-code');
  let rooturl = $(element).data('rooturl');

  $.ajax({
    type: 'GET',
    url: ''+rooturl+'declaration/'+trackingCode+'/',
    data: {},
    cache: false,
    beforeSend(){
      $("#declar-form-"+trackingCode).fadeIn().html('<div class="preloader-cog"><i class="fa fa-cog fa-spin fa-4x text-danger"></i></div>');
    },
    success: function(data){
      $("#declar-form-"+trackingCode).html(data);
      $("#cancel-"+trackingCode).fadeIn();
      $("#save-"+trackingCode).fadeIn();
      $(element).hide();
      masonryPackage();
    }
  });
};

let cancelDeclar = (element) => {
  let trackingCode = $(element).data('tracking-code');
  $("#declar-form-"+trackingCode).fadeOut(function(){
    $("#declar-form-"+trackingCode+" form").remove();
    $("#declar-btn-"+trackingCode).fadeIn();
    $("#cancel-"+trackingCode).hide();
    $("#save-"+trackingCode).hide();
    masonryPackage();
  });
};
let triggerSubmit = (element) => {
  let packageid = $(element).data("packageid");
  $("#submit-button-"+packageid).trigger("click");
};

let updatePackage = (element) => {
  let rooturl = $(element).data("rooturl");
  let packageid = $(element).data("packageid");
  let subForm = $("#declaration-form-"+packageid);

  subForm.on('submit', function(event){
      console.log("AFTER FORM");
      let form = $(this);
      let formdata = new FormData(this);

      $.ajax({
        url         : ''+rooturl+'updatepackage/?packageid='+packageid+'',
        data        : formdata ? formdata : form.serialize(),
        cache       : false,
        contentType : false,
        processData : false,
        type        : 'POST',
        beforeSend  : function(){
          $("#package-inner-"+packageid)
              .append("<div class='dim-form'><i class='fa fa-cog fa-spin fa-4x' style='color:white;'></i></div>");
        },
        success     : function(data){
          console.log(data);
          $("#package-outer-"+packageid).load(rooturl + "packagedata/?status=&start=0&amount=100 #package-inner-"+packageid);
          $('.message-place').load(document.URL +  ' .message-display', function(){
            $(".message-display").hide().fadeIn();
            message_fade_out();
          });
          setTimeout(function(){
            masonryPackage();
          }, 500);
        }
      });
      event.preventDefault();
      event.stopImmediatePropagation();
  });
};

let volumeCalculator = (element) => {
  let rooturl = $(".weightForm").data("rooturl");

  $.post(rooturl+"volumecalculator/", $(".weightForm :input").serialize(), function(data){
    $("#value-calculator").html(data);
  });
};

let currencyCalculator = (element) => {
  let rooturl = $(".weightForm").data("rooturl");

  $.post(rooturl+"currencycalculator/", $(".currencyForm :input").serialize(), function(data){
    $("#currency-calculator").html(data);
  });
};

let printdiv = (printpage) => {
  let headstr = "<html><head><title></title></head><body>";
  let footstr = "</body></html>";
  let newstr = document.all.item(printpage).innerHTML;
  let oldstr = document.body.innerHTML;
  document.body.innerHTML = headstr+newstr+footstr;
  window.print();
  document.body.innerHTML = oldstr;
  return false;
};

let gotoinvoice = (element) => {
  if($(element).is(':checked')){
    $("#invoice-form").attr("action", $(element).data("rooturl") + "account/invoice/" + $(element).val() + "/");
    $("#invoice-form-button").prop("disabled", false);
  }else{
    $("#invoice-form").attr("action", "");
    $("#invoice-form-button").prop("disabled", true);
  }
};

// let getRegister = (element) => {
//   let rooturl = $(element).data("rooturl");
//   let registerType = $(element).data("register-type");
//
//   $.ajax({
//     type: 'GET',
//     url: rooturl+registerType,
//     data: {},
//     cache: false,
//     beforeSend(){
//       $("#"+registerType).html('<div class="preloader-cog"><i class="fa fa-cog fa-spin fa-4x text-danger"></i></div>');
//     },
//     success: function(data){
//       $("#"+registerType).html(data);
//     }
//   });
// };

let removePadding = () => {
  setTimeout(function(){
    $(".modal").css('padding-right','');
  },1);
};

let agreeRules = (element) => {
  let register = $("#register");
  if($(element).is(':checked')){
    register.removeClass("btn-danger");
    register.addClass("btn-success");
    register.prop("disabled", false);
  }else{
    register.removeClass("btn-success");
    register.addClass("btn-danger");
    register.prop("disabled", true);
  }
};

let changeEntrepreneur = (element) => {
  if($(element).is(":checked")){
    $("#legal-person-name-container").hide();
    $("#legal-person-name").prop("disabled", true);
    $("#entrepreneur-document-upload").fadeIn();
    $("#entrepreneur-document-input").prop("disabled", false);
  }else{
    $("#legal-person-name-container").fadeIn();
    $("#legal-person-name").prop("disabled", false);
    $("#entrepreneur-document-upload").hide();
    $("#entrepreneur-document-input").prop("disabled", true);
  }
};