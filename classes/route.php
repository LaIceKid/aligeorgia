<?php
class Route extends Model{
  public function router(){
    $route = array(
		'' => array('controller' => 'home', 'action' => 'index'),
		'login' => array('controller' => 'user', 'action' => 'login'),
		'account' => array('controller' => 'user', 'action' => 'account'),
	    'register' => array('controller' => 'user', 'action' => 'register'),
	    'physicalperson' => array('controller' => 'user', 'action' => 'physicalperson'),
	    'legalentity' => array('controller' => 'user', 'action' => 'legalentity'),
		'checklogin' => array('controller' => 'user', 'action' => 'checklogin'),
		'checkregister' => array('controller' => 'user', 'action' => 'checkregister'),
		'logout' => array('controller' => 'user', 'action' => 'logout'),
		'about' => array('controller' => 'about', 'action' => 'index'),
		'contact' => array('controller' => 'contact', 'action' => 'index'),
		'schedule' => array('controller' => 'schedule', 'action' => 'index'),
		'shops' => array('controller' => 'shops', 'action' => 'index'),
		'language' => array('controller' => 'language', 'action' => 'select'),
	    'insertpackage' => array('controller' => 'packages', 'action' => 'insertpackage'),
	    'updatepackage' => array('controller' => 'packages', 'action' => 'updatepackage'),
	    'packagedata' => array('controller' => 'packages', 'action' => 'packagedata'),
		'packagedataflight' => array('controller' => 'packages', 'action' => 'packagedataflight'),
	    'declaration' => array('controller' => 'packages', 'action' => 'declaration'),
	    'volumecalculator' => array('controller' => 'home', 'action' => 'volumecalculator'),
	    'currencycalculator' => array('controller' => 'home', 'action' => 'currencycalculator'),
	    'topup' => array('controller' => 'user', 'action' => 'topup'),
	    'balancemanipulations' => array('controller' => 'user', 'action' => 'balancemanipulations')
    );
    return $route;
  }
}
