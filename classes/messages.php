<?php
class Messages{
	public static function setMsg($text, $type){
		if($type == 'error'){
			$_SESSION['errorMsg'] = $text;
		}else{
			$_SESSION['successMsg'] = $text;
		}
	}

	public static function display(){
		$langs = new LanguageModel();
		$lang = $langs->SelectLanguage();

		if(isset($_SESSION['errorMsg'])){
			foreach($_SESSION['errorMsg'] as $erMsg){
				$errorMessage[] = $lang[$erMsg];
			}
			echo '<div class="alert alert-danger message-display">'.implode('<br>', $errorMessage).'</div>';
			unset($_SESSION['errorMsg']);
		}
		if(isset($_SESSION['successMsg'])) {
			foreach ($_SESSION['successMsg'] as $succMsg) {
				$successMessage[] = $lang[$succMsg];
			}

			echo '<div class="alert alert-success message-display">' . implode('<br>', $successMessage). '</div>';
			unset($_SESSION['successMsg']);
		}
	}
}
