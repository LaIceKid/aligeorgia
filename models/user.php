<?php
class UserModel extends Model{
  public function login(){
    if(empty($_POST)){
      return;
    }
    $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    $this->query("SELECT * FROM users WHERE id = :id");
    $this->bind("id", Other::axCodeReverse($_POST['axcode']));
    $row = $this->single();
	  $succMsg = array();
	  $errMsg = array();
    if($row){
      if($row['password'] == md5($post['password'])){
        unset($row['password']);
        if(empty($row['avatar'])){
        	$row['avatar'] = 'no-avata.svg';
        }
        $_SESSION['user_data'] = $row;
        $succMsg[] = "LOGIN_SUCCESS";
        Messages::setMsg($succMsg, "success");
        header("location:".ROOT_URL."account/");
        return;
      }else{
        $errMsg[] = "LOGIN_INCORRECT_PASSWORD";
        Messages::setMsg($errMsg, "error");
      }
    }else{
        $errMsg[] = "LOGIN_USER_DOES_NOT_EXISTS";
      Messages::setMsg($errMsg, "error");
    }
    header("location:".ROOT_URL);
  }


  public function logout(){
    if(isset($_SESSION['user_data'])){
      unset($_SESSION['user_data']);
      $succMsg = array("LOGOUT_SUCCESS");
      Messages::setMsg($succMsg, "success");
    }
    header("location:".ROOT_URL);
    return;
  }

  public function account(){
    if(empty($_SESSION['user_data'])){
      header("location:".ROOT_URL);
      return;
    }
  }

  public function balancemanipulations(){
	$errors = array();
	$successMsgs = array();
	  if(!isset($_GET['key'])){
		  $errors[] = 'TOPUP_IS_IMPOSSIBLE';
	  }
	  if($_GET['key'] == '4297f44b13955235245b2497399d7a93'){
		  $errors[] = 'TOPUP_IS_IMPOSSIBLE';
	  }
	  if(!isset($_SESSION['user_data']['id'])){
		  $errors[] = 'LOGIN_BEFORE_TOPUP';
	  }
	  if($status == 'failed'){
		  $errors[] = 'TOPUP_IS_IMPOSSIBLE';
		  unset($_SESSION['balance']);
		  return;
	  }
	  if($status == 'success'){
		  if(!isset($_SESSION['balance'])){
			  $errors[] = 'LOGIN_BEFORE_TOPUP';
		  }

		  if(!empty($errors)){
		  	Messages::setMsg($errors, 'error');
		  	return;
		  }

		  $newBalance = substr_replace($_SESSION['balance'],'.',-2,-2);

		  $this->query("SELECT * FROM balance WHERE userID = :userID");
		  $this->bind("userID", $_SESSION['user_data']['id']);
		  $row = $this->single();

		  if(empty($row)){
			  $this->query("INSERT INTO balance (balance, userID) VALUES (:newBalance, :userID)");
			  $this->bind("newBalance", $newBalance);
			  $this->bind("userID", $_SESSION['user_data']['id']);
			  $this->execute();
		  }else{
			  $totalBalance = $row['balance'] + $newBalance;
			  $this->query("UPDATE balance SET balance = :balance WHERE userID = :userID");
			  $this->bind("balance", $totalBalance);
			  $this->bind("userID", $_SESSION['user_data']['id']);
			  $this->execute();
		  }

//		  $newBalance
//		  $totalBalance
		  $successMsgs[] = "TOPUP_SUCCEED";
		  Messages::setMsg($successMsgs, 'success');

		  unset($_SESSION['balance']);
	  }
  }

  public function topup(){
	  $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

	  $errors = array();
	  $pay = true;

		foreach ($post as $key => $item){
			if(empty($item)){
				if($key == 'tetri'){
					$post[$key] = '00';
				}
				if($key == 'lari'){
					$post[$key] = '0';
				}
				$fields['amount'][] = 'EMPTY_VALUE_'.strtoupper($key);
			}
		}

	  if (strlen($post['tetri']) != 2) {
		  $pay = false;
		  $errors[] = 'TETRI_MUST_BE_TWO_CHARACTERS';
	  }

		if(isset($fields['amount'])){
			if(count($fields['amount']) == 2){
				$pay = false;
				$errors[] = 'PAYMENT_ERROR_ALL_VALUES_EMPTY';
			}
		}

//	  echo '<pre>';
//	  print_r($post);
//	  echo '<hr>';
//	  print_r($errors);
//	  echo '</pre>';

	  if($pay){
		  $orderID = md5(uniqid(rand(), true));
		  $pay_day = date('Ymd H:m:i');
		  $amount = $post['lari'] . $post['tetri'];
		  $_SESSION['balance'] = $amount;
		  header('location:https://3dacq.georgiancard.ge/payment/start.wsm?page_id=B1069DCF8E094A406F0A5C82684652CA&account_id_gel=CEAA21277C5182E7AE1DE136F8A1558D&trx_id=E5BA19944804390A51241B53BC716E62&lang=ka&merch_id=066782F7975C5A14ED8B41B425D1E080&ccy=981&o.amount=' . $amount . '&o.order_id=' . $orderID . '&o.axcode=' . $_SESSION['user_data']['id'] . '&ts=' . $pay_day . '&back_url_s=http://aligeorgia.ge/payments/success.php&back_url_f=http://aligeorgia.ge/payments/failed.php');
	  }else{
	  	Messages::setMsg($errors, 'error');
	  	header("location:".ROOT_URL."account/topup/");
	  }
  }

	public function balanceView(){
		$this->query('SELECT * FROM balance WHERE userID = "'.$_SESSION['user_data']['id'].'"');
		$rows = $this->resultSet();
		if(count($rows) == 0){
			$rows = '0';
		}else{
			foreach($rows as $ebal){
				$bal[] = $ebal['balance'];
			}
			$rows = $bal[0];
		}
		return $rows;
	}

	public function invoice($flightID = NULL, $userID = NULL){
		$this->query("
			SELECT p.*, p.id as prodID, u.*, f.*
			FROM products p
			LEFT JOIN users u
			ON p.userID = u.id
			LEFT JOIN flight f
			ON p.flightID = f.id
			WHERE p.flightID = :fid AND p.userID = :uid"
		);
		$this->bind("fid", $flightID);
		$this->bind("uid", $userID);
		$rows = $this->resultSet();
		return $rows;
	}

	public function pay(){
		$this->query("SELECT * FROM balance WHERE userID = :userID");
		$this->bind("userID", $_SESSION['user_data']['id']);
		$rows = $this->resultSet();
		if($_SESSION['pay']['totalPrice'] == 0){
			Messages::setMsg('გადახდასთან დაკავშირებით დაფიქსირდა შეცდომა. გთხოვთ დაგვიკაშირდით', 'error');
		}elseif($rows[0]['balance'] >= $_SESSION['pay']['totalPrice']){
			$balance = $rows[0]['balance'] - $_SESSION['pay']['totalPrice'];

			$this->query("UPDATE balance SET balance = :balance WHERE userID = :userID");
			$this->bind("balance", $balance);
			$this->bind("userID", $_SESSION['user_data']['id']);
			$this->execute();

			$prodIDs = explode(',', $_SESSION['pay']['product']);
			foreach($prodIDs as $ids){
				$this->query("UPDATE products SET state = 'paid' WHERE id = $ids");
				$this->execute();
			}
			Messages::setMsg('ინვოისი გადახდილია', 'success');
		}else{
			Messages::setMsg('თქვენ არ გაქვთ საკმარისი თანხა ბალანსზე', 'error');
		}
	}

	public function invNumb($userID, $flightID){
		$this->query('SELECT * FROM invoice WHERE userID = "'.$userID.'" AND flightID = "'.$flightID.'"');
		$row = $this->single();
		echo $row['id'];
	}

	public function register(){
		$this->legalentity();
	}

	public function physicalperson(){

	}

	public function legalentity(){
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$errors = array();
		if(isset($post) && !empty($post)){
			$keyz = array();
			foreach($post as $key => $item){
				if(empty($item)){
					$errors[] = strtoupper($key).'_EMPTY_FIELD';
				}
			}
		}

		$birthday = $post['year'].'-'.$post['month'].'-'.$post['day'];
		$nameSurname = $post['name'].' '.$post['surname'];
		$nameSurnameEn = $post['nameEn'].' '.$post['surnameEn'];
		$password = MD5($_POST['password']);
		$entrepreneur_document_name = $this->documentUpload();

		if(!empty($errors)){
			Messages::setMsg($errors, "error");
			return;
		}
		$this->query("INSERT INTO users (name, nameEn, email, password, identification_code, passport, address, birthday, phone) VALUES (:name, :nameEn, :email, :password, :legal_person_name, :identification_code, :passport, :address, :birthday, :phone, :entrepreneur_document)");
		$this->bind('name', $nameSurname);
		$this->bind('nameEn', $nameSurnameEn);
		$this->bind('email', $post['email']);
		$this->bind('entrepreneur_document', $entrepreneur_document_name);
		$this->bind('identification_code', $identification_code);
		$this->bind('legal_person_name', $legal_person_name);
		$this->bind('password', $password);
		$this->bind('passport', $passport);
		$this->bind('address', $post['address']);
		$this->bind('birthday', $birthday);
		$this->bind('phone', $post['phone']);
		$this->execute();
		$success[] = "REGISTER_SUCCESS";
		Messages::setMsg($success, "success");
		return;
	}

	public function documentUpload(){
		$filename = '-';
		if(!empty($_FILES['entrepreneur_document']['name'])){
			$error = 0;
			// ერორებზე შემოწმება
			if($_FILES['entrepreneur_document']['error'] > 0){
				Messages::setMsg('ატვირთვის დროს მოხდა შეცდომა', 'error');
				$error = 1;
			}

			// ზომის შემოწმება
			if($_FILES['entrepreneur_document']['size'] > 1000000){
				Messages::setMsg('ფაილის ზომა არ უნდა აღემატებოდეს <b>1</b> მეგაბაიტს', 'error');
				$error = 2;
			}

			// ატვირთვა
			if($error == 0){
				$fileName = MD5(time()).'.'.pathinfo($_FILES['entrepreneur_document']['name'], PATHINFO_EXTENSION);
				if(!move_uploaded_file($_FILES['entrepreneur_document']['tmp_name'], '/home/airexge/domains/aligeorgia.ge/public_html/test/entrepreneur_documents/'.$fileName)){
					Messages::setMsg('ფაილის ატვირთვა შეუძლებელია, შეამოწმეთ დირექტორია.', 'error');
				}else{
					return $fileName;
				}
			}
		}else{
			return $filename;
		}
	}

}
