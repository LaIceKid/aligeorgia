<?php
class PackagesModel extends Model{
  public function awaiting(){
  	$this->query("SELECT * FROM products WHERE userID = :userID AND status = ''");
  	$this->bind("userID", $_SESSION['user_data']['id']);
  	$rows = $this->resultSet();
  	return $rows;
  }
  public function warehouse(){
	  $this->query("SELECT * FROM products WHERE userID = :userID AND status = 'china-stock'");
	  $this->bind("userID", $_SESSION['user_data']['id']);
	  $rows = $this->resultSet();
	  return $rows;
  }
  public function pending(){
	  $this->query("SELECT * FROM products WHERE userID = :userID AND status = 'sent' ORDER BY flightID DESC");
	  $this->bind("userID", $_SESSION['user_data']['id']);
	  $rows = $this->resultSet();
	  $result = array();
	  foreach($rows as $item){
		  $result[$item['flightID']][$item['id']] = $item;
	  }
	  return $result;
  }
  public function arrived(){
	  $this->query("SELECT * FROM products WHERE userID = :userID AND status = 'arrived'");
	  $this->bind("userID", $_SESSION['user_data']['id']);
	  $rows = $this->resultSet();
	  return $rows;
  }
  public function obtained(){
	  $this->query("SELECT * FROM products WHERE userID = :userID AND status = 'obtained'");
	  $this->bind("userID", $_SESSION['user_data']['id']);
	  $rows = $this->resultSet();
	  return $rows;
  }

	public function add_item(){
		$this->query("SELECT * FROM products WHERE userID = :userID AND status = 'obtained'");
		$this->bind("userID", $_SESSION['user_data']['id']);
		$rows = $this->resultSet();
		return $rows;
	}

	public function packagedata(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

		$this->query("SELECT * FROM products WHERE userID = :userID AND status = :status");
		$this->bind("status", $get['status']);
		$this->bind("userID", $_SESSION['user_data']['id']);
		$rows = $this->resultSet();
		foreach($rows as $key => $row){
			$rows[$key]['comment'] = $this->select_comment($row['productOrder']);
		}
		return $rows;
	}

	public function packagedataflight(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

		$this->query("SELECT * FROM products WHERE userID = :userID AND status = :status ORDER BY flightID DESC");
		$this->bind("status", $get['status']);
		$this->bind("userID", $_SESSION['user_data']['id']);
		$rows = $this->resultSet();
		$result = array();
		foreach($rows as $item){
			$item['comment'] = $this->select_comment($item['productOrder']);
			$result[$item['flightID']][$item['id']] = $item;
		}
		return $result;
	}

	public function declaration(){
//		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
//		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
//
//		$this->query("UPDATE products SET productName = :name, quantity = :quantity, price = :price, currency = :currency, shop = :shop, declar = :declar, create_date = :create_date, file = :file WHERE id = :id");
//		$this->bind(':name', $post['name']);
//		$this->bind(':quantity', $post['quantity']);
//		$this->bind(':shop', $post['shop']);
//		$this->bind(':declar', 'user');
//		$this->bind(':price', $post['price']);
//		$this->bind(':currency', $post['currency']);
//		$this->bind(':id', $post['id']);
//		$this->bind(':create_date', date('Y-m-d H:i:s'));
//		$this->bind(':file', $this->fileUpload($_SESSION['user_data']['id']));
//		$this->execute();
	}

	public function upload(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$uploadOk = 1;
		$separate_filename = '';
		if(isset($_FILES['files']) && !empty($_FILES['files'])){
			$i = 0;
			$errors = array();
			$errors['file-types'] = $post;
			$errors['upload-ok'] = $uploadOk;
			foreach($_FILES['files']['name'] as $fKey => $file){
				if($_FILES['files']["error"][$i] != 0){
					$errors['errors'][] = "FOLDER_OR_EMPTY_FORM_SUBMITTED";
					$uploadOk = 0;
				}
				$target_dir = 'assets/images/product_images/';
				$separate_filename = time().strtolower(str_replace(' ', '-', basename($_FILES['files']["name"][$i])));

				$target_file = $target_dir.$separate_filename;

				$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

				$separate_filename = md5($separate_filename).'.'.$imageFileType;
				$target_file = $target_dir.$separate_filename;
				// Check if image file is a actual image or fake image
				$check = NULL;
				if(!empty($_FILES['files']["tmp_name"][$i])){
//			        $check = getimagesize($_FILES['files']["tmp_name"][$i]);
				}
				if($check !== false) {
					//$errors['errors'][] = $_FILES['files']['name'][$i]." file is an image - " . $check["mime"] . ".";
					$uploadOk = 1;
				} else {
					$errors['errors'][] = "FILE_IS_NOT_AN_IMAGE";
					$uploadOk = 0;
				}

				// Check file size
				if ($_FILES['files']["size"][$i] > 1000000) {
					$errors['errors'][] = "FILE_IS_TOO_LARGE";
					$uploadOk = 0;
				}
				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
					&& $imageFileType != "gif" && $imageFileType != "pdf" ) {
					$errors['errors'][] = "ONLY_FILE_TYPES_ALLOWED";
					$uploadOk = 0;
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
					$errors['upload-ok'] = $uploadOk;
					// if everything is ok, try to upload file
				} else {
					if (move_uploaded_file($_FILES['files']["tmp_name"][$i], $target_file)) {
						$uploadOk == 0;
						$errors['errors'][] = "CANT_MOVE_FILE";
					}
				}
				//print_r($errors);
				$i++;
			}
			if($uploadOk == 0){
				$errors['errors'][] = "ONE_OR_MORE_FILES_NOT_UPLOADED";
				$errors['errors'] = array_reverse($errors['errors']);
				Messages::setMsg($errors['errors'], 'error');
			}
		}

		if($uploadOk == 1){
			$uploadOk = $separate_filename;
		}
		return $uploadOk;
	}

	public function convertToReadableSize($size){
		$base = log($size) / log(1024);
		$suffix = array("", "KB", "MB", "GB", "TB");
		$f_base = floor($base);
		return round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
	}

	public function insertpackage($array = array(), $filename = NULL){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$array = $post;
		$filename = $this->upload();

  	    $errors = array();
		foreach($array as $key => $item){
			if(empty($item)){
				$errors[] = 'SIGN_ALL_FIELDS';
			}
			if($key == 'quantity'){
				if(!is_numeric($item)){
					$errors[] = 'QUANTITY_WITH_NUMBERS_ONLY';
				}
			}
			if($key == 'price'){
				if(!is_numeric($item)){
					$errors[] = 'QUANTITY_WITH_NUMBERS_ONLY';
				}
			}
		}
		if(empty($errors)){
			$this->query('INSERT INTO products (productOrder, productName, shop, quantity, price, currency, userID, buyerName, phone, passport, file, buyerNameEng, create_date, declar) VALUES (:productOrder, :name, :shop, :quantity, :price, :currency, :userID, :userName, :phone, :passport, :file, :nameEn, :create_date, :declar)');
			$this->bind(':productOrder', $array['productOrder']);
			$this->bind(':name', $array['name']);
			$this->bind(':shop', $array['shop']);
			$this->bind(':quantity', $array['quantity']);
			$this->bind(':price', $array['price']);
			$this->bind(':create_date', $array['dateTime']);
			$this->bind(':currency', $array['currency']);
			$this->bind(':userID', $_SESSION['user_data']['id']);
			$this->bind(':userName', $_SESSION['user_data']['name']);
			$this->bind(':phone', $_SESSION['user_data']['phone']);
			$this->bind(':passport', $_SESSION['user_data']['passport']);
			$this->bind(':file', $filename);
			$this->bind(':nameEn', $_SESSION['user_data']['nameEn']);
			$this->bind(':declar', 'user');
			$this->execute();
			$lastID = $this->lastInsertId();
			if($lastID != 0){
				Messages::setMsg(array('0' => 'ADD_PRODUCT_SUCCESS'), 'success');
			}else{
				Messages::setMsg(array('0' => 'ADD_PRODUCT_ERROR'), 'error');
			}
		}

		header("location:".ROOT_URL."account/add-item/");
		return $lastID; //JUST IN CASE...
	}

	public function updatepackage($array = array(), $filename = NULL){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		print_r($post);
		print_r($_FILES);
		$array = $post;
		echo $filename = $this->upload();

		$errors = array();
		foreach($array as $key => $item){
			if(empty($item)){
				$errors[] = 'SIGN_ALL_FIELDS';
			}
			if($key == 'quantity'){
				if(!is_numeric($item)){
					$errors[] = 'QUANTITY_WITH_NUMBERS_ONLY';
				}
			}
			if($key == 'price'){
				if(!is_numeric($item)){
					$errors[] = 'QUANTITY_WITH_NUMBERS_ONLY';
				}
			}
		}
		$lastID = 0;
		if(empty($errors)){
			$this->query("UPDATE products SET 
                    productName = :productName, 
                    shop = :shop, 
                    quantity = :quantity, 
                    price = :price, 
                    currency = :currency, 
                    userID = :userID, 
                    buyerName = :userName, 
                    phone = :phone, 
                    passport = :passport, 
                    file = :file, 
                    buyerNameEng = :buyerNameEng, 
                    create_date = :create_date, 
                    declar = :declar WHERE 
					productOrder = :productOrder
			");
			$this->bind('productOrder', $array['productOrder']);
			$this->bind('productName', $array['name']);
			$this->bind('shop', $array['shop']);
			$this->bind('quantity', $array['quantity']);
			$this->bind('price', $array['price']);
			$this->bind('create_date', $array['dateTime']);
			$this->bind('currency', $array['currency']);
			$this->bind('userID', $_SESSION['user_data']['id']);
			$this->bind('userName', $_SESSION['user_data']['name']);
			$this->bind('phone', $_SESSION['user_data']['phone']);
			$this->bind('passport', $_SESSION['user_data']['passport']);
			$this->bind('file', $filename);
			$this->bind('buyerNameEng', $_SESSION['user_data']['nameEn']);
			$this->bind('declar', 'user');
			$this->execute();
			$lastID = $this->rowCount();
			if($lastID != 0){
				Messages::setMsg(array('0' => 'DECLAR_SUCCESS'), 'success');
			}else{
				Messages::setMsg(array('0' => 'DECLAR_ERROR'), 'error');
			}
		}else{
			echo 'asd';
		}
		return $lastID; //JUST IN CASE...
	}


	public function selectProduct(){
		if(isset($_SESSION['user_data']['id'])){
			$this->query('
				SELECT * FROM flight
				RIGHT JOIN products
				ON flight.id = products.flightID
				WHERE userID = '.$_SESSION['user_data']['id'].'
				ORDER BY products.id DESC
				');
			$rows = $this->resultSet();
			return $rows;
		}
		return 0;
	}

	public function select_comment($tracking_code = NULL){
  	    $this->query("SELECT * FROM product_comments WHERE tracking_code = :tracking_code");
  	    $this->bind("tracking_code", $tracking_code);
  	    return $this->single();
	}
}
