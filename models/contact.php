<?php
class ContactModel extends Model{
	public function Index(){
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		if(isset($_POST['g-recaptcha-response'])){
			if(empty($_POST['g-recaptcha-response'])){
				Messages::setMsg('გთხოვთ მონიშნეთ ველი სადაც ადასტურებთ, რომ არ ხართ რობოტი.', 'error');
				return;
			}
		}
		if(strlen($post['phone']) >= 10){
			Messages::setMsg('მოხდა შეცდომა. გთხოვთ დაუკავშირდით ადმინისტრატორს ტელეფონის ნომერზე', 'error');
			return;
		}
		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .=	'Reply-To: ' . $post['mail'] . "\r\n";
		$headers .= 'From: <AliGeorgia>' . "\r\n";
		$headers .= 'Cc: AliGeorgia' . "\r\n";
		if(isset($post['sent'])){
			if(!empty($post['nameSurname']) &&!empty($post['mail']) &&!empty($post['phone']) &&!empty($post['question'])){		
				$mailName = 'info.aligeorgia@gmail.com';
				$msg = '
					Dear: AliGeorgia Administration <br/><br/>
					Name Surname: '.$post['nameSurname'].' <br/>
					Email: '.$post['mail'].'<br/>
					Phone: '.$post['phone'].'<br/>
					Question: '.$post['question'].'<br/>
				';
				mail($mailName,"Question",$msg,$headers);
				Messages::setMsg('თქვენი კითხვა გაიგზავნა', 'success');
				return;
			}else{
				Messages::setMsg('შეავსეთ ყველა ველი', 'error');
				return;
			}
		}
		
	}
}