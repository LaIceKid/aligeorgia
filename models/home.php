<?php
class HomeModel extends Model{
	public function index(){
		$homeArray = array();
		$homeArray['fromtPageSchedule'] = $this->frontPageSchedule();
		return $homeArray;
	}

	public function frontPageSchedule(){
		$this->query('SELECT * FROM flight ORDER BY id DESC LIMIT 4');
		$rows = $this->resultSet();
		return $rows;
	}

	public function volumecalculator(){
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$errors = array();

		foreach($post as $key => $item){
			if(empty($item)){
				$errors[] = 'Empty values provided...';
			}
		}
		if(!empty($errors)){
			echo 0; // json_encode($errors);
			return;
		}
		echo json_encode($post['length'] * $post['width'] * $post['height'] / 6000);
	}

	public function currencycalculator(){
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

		if(isset($post) && !empty($post)){
			$client = new SoapClient('http://nbg.gov.ge/currency.wsdl');

			$result = $_POST['curr'] * $client->GetCurrency('CNY') / 10;
			echo $result;
			return $result;
		}
		echo 'Error: data is not provided...';
	}

}
