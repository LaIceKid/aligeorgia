<?php
class Currency extends Controller{
	protected function select(){
		$viewmodel = new CurrencyModel();
		$this->ReturnView($viewmodel->select(), true);
	}

	protected function download(){
		$viewmodel = new CurrencyModel();
		$this->ReturnView($viewmodel->download(), false);
	}
}
