<?php
class User extends Controller{
  protected function login(){
    $viewmodel = new UserModel;
    $viewmodel->login();
  }
  protected function logout(){
    $viewmodel = new UserModel;
    $viewmodel->logout();
  }
  protected function register(){
    $viewmodel = new UserModel;
    $this->ReturnView($viewmodel->register(), true);
  }
  protected function account(){
    $viewmodel = new UserModel;
    $this->ReturnView($viewmodel->account(), true);
  }
	protected function topup(){
		$viewmodel = new UserModel;
		$viewmodel->topup();
	}
	protected function balancemanipulations(){
		$viewmodel = new UserModel;
		$viewmodel->balancemanipulations();
	}
	protected function physicalperson(){
		$viewmodel = new UserModel;
		$this->ReturnView($viewmodel->physicalperson(), false);
	}
	protected function legalentity(){
		$viewmodel = new UserModel;
		$this->ReturnView($viewmodel->legalentity(), false);
	}
}
