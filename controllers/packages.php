<?php
class Packages extends controller{
	protected function insertpackage(){
		$viewmodel = new PackagesModel();
		$viewmodel->insertpackage();
	}
	protected function updatepackage(){
		$viewmodel = new PackagesModel();
		$viewmodel->updatepackage();
	}
	public function packagedata(){
		$viewmodel = new PackagesModel();
		$this->ReturnView($viewmodel->packagedata(), false);
	}
	public function packagedataflight(){
		$viewmodel = new PackagesModel();
		$this->ReturnView($viewmodel->packagedataflight(), false);
	}
	public function declaration(){
		$viewmodel = new PackagesModel();
		$this->returnView($viewmodel->declaration(), false);
	}
}