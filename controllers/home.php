<?php
class Home extends Controller{
	protected function index(){
		$viewmodel = new HomeModel();
		$this->ReturnView($viewmodel->index(), true);
	}

	protected function volumecalculator(){
		$viewmodel = new HomeModel();
		$viewmodel->volumecalculator();
	}
	protected function currencycalculator(){
		$viewmodel = new HomeModel();
		$viewmodel->currencycalculator();
	}
}
