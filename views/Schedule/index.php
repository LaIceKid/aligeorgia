<?php $Other = new Other(); ?>
<br /><br />
<h3 class="schedule-title">რეისების განრიგი</h3>
<br />

<div class="table-responsive">
	<table class="table table-hover flight-table">
		<thead class="thead-dark">
			<tr>
				<th class="flightRed">
					რეისის ნომერი
				</th>
				<th class="flightRed">
					გამოფრენა
				</th>
				<th class="flightRed">
					სავარაუდო თარიღი
				</th>
				<th class="flightRed">
					ჩამოფრენის თარიღი
				</th>
			</tr>
		</thead>

		<?php
			foreach($viewmodel as $sched):
		?>
		<tr>
			<td>
				<?php echo $Other->flight_id($sched['id']); ?>
			</td>
			<td>
				<?php if($sched['startDate'] == '' || $sched['startDate'] == 0){ ?>
					<?php echo '--/--/'.date("Y"); ?>
				<?php }else{ ?>
					<?php echo date('d/m/Y', strtotime($sched['startDate'])); ?>
				<?php } ?>
			</td>
			<td>
				<?php if($sched['date'] == '' || $sched['date'] == 0){ ?>
					<?php echo '--/--/'.date("Y"); ?>
				<?php }else{ ?>
					<?php echo date('d/m/Y', strtotime($sched['date'])); ?>
				<?php } ?>
			</td>
			<td>
				<?php if($sched['endDate'] == '' || $sched['endDate'] == 0){ ?>
					<?php echo '--/--/'.date("Y"); ?>
				<?php }else{ ?>
					<?php echo date('d/m/Y', strtotime($sched['endDate'])); ?>
				<?php } ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
</div>
