<br><br>
<h3 class="contact-title">კონტაქტი</h3><br>
<div class="container contacts-container">
	<div class="row">
		<div class="col-md-5">
			<b class="caps">სამუშაო საათები</b><br>
			<i class="fas fa-clock"></i> ორშაბათი-პარასკევი: 11:00-19:00 (შაბათი 11:00-14:00)<br>
			<hr/>
			<b class="caps">მისამართი:</b><br>
			<i class="fas fa-map-marker-alt"></i> ქ.თბილისი, უშანგი ჩხეიძის #19<br>
			<i class="fas fa-mail-bulk"></i> info.aligeorgia@gmail.com<br>
			<i class="fas fa-phone"></i> (+995) 032 2 196 191<br>
			<hr/>
			<a class="btn btn-primary" href="skype:aligeorgia.?chat"><i class="fab fa-skype"></i> aligeorgia.</a> <a class="btn-facebook-custom" href="https://www.facebook.com/AliGeorgia.ge/" target="_blank"><i aria-hidden="true" class="fab fa-facebook-square" style="font-size:25px;"></i> acebook</a>
			<hr/>
			<form action="" method="post">
				<div class="form-group">
					<input aria-describedby="nameSurnameHelp" class="form-control" name="nameSurname" placeholder="სახელი და გვარი" type="text">
				</div>
				<div class="form-group">
					<input aria-describedby="emailHelp" class="form-control" name="mail" placeholder="ელ. ფოსტა" type="email">
				</div>
				<div class="form-group">
					<input aria-describedby="emailHelp" class="form-control" name="phone" placeholder="ტელეფონის ნომერი" type="text">
				</div>
				<div class="form-group">
					<textarea aria-describedby="emailHelp" class="form-control" id="exampleInputEmail1" name="question" placeholder="შეიყვანეთ ტექტსი" rows="10"></textarea>
				</div>
				<div class="form-group">
					<div class="g-recaptcha" data-sitekey="6LfdQVEUAAAAAKCRJEjIhfAvSjbvii_DCMu_4x8B"></div>
				</div>
				<div class="form-group">
					<input aria-describedby="emailHelp" class="form-control btn btn-success" name="sent" type="submit" value="გაგზავნა">
				</div>
			</form>
		</div>
		<div class="col-md-7">
			<iframe allowfullscreen frameborder="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d23829.00090797919!2d44.79032500000001!3d41.70702800000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5b9b36c684888d8c!2sALIGEORGIA!5e0!3m2!1sen!2sge!4v1484161947567" style="border:0;width:100%;height:100%;"></iframe>
		</div>
	</div>
</div>
