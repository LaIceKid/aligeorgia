<?php $ShopsModel = new ShopsModel; ?>
<br/><br/>
<div class="container shop-container">
	<h3 class="shops-title">მაღაზიები</h3>
	<hr />
  <div class="row">
		<?php if(count($viewmodel) >= 1){ // SHOPS VIEW ?>
			<?php	foreach($viewmodel as $shop){ ?>
				<div class="card col-md-3 shop">
					<div class="shop-img">
						<img class="card-img-top" src="<?php echo $shop['image']; ?>">
					</div>
				  <div class="card-body">
				    <h4><a class="card-text" href="<?php echo $shop['link']; ?>" target="_blank"><?php echo $shop['name']; ?></a></h4>
				  </div>
				</div>
			<?php } ?>
		<?php } ?>
	</div>
</div>

<br />
<br />

<br />
<br />

<div class="container">
	<h3 class="video-title">ვიდეო</h3>
	<hr />
  <div class="row">
		<?php foreach($ShopsModel->videos() as $item){ // VIDEOS VIEW ?>
			<div class="col-md-4">
				<div class="card border-0">
					<div class="card-content">
							<span class="card-title"><h4 class="text-truncate"><?php echo $item['title']; ?></h4></span>
					</div>

					<div class="card-image">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php echo $item['video_id']; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
