<?php if($_GET['status'] == 'arrived'){ ?>
    <form action="" method="post" id="invoice-form">
        <input type="submit" name="invoice" id="invoice-form-button" value="მონიშნულის ინვოისი/გადახდა" class="btn btn-success" disabled>
    <br>
    <br>
<?php } ?>
<div class="package-main-container">
	<?php foreach($viewmodel as $flightID => $packageData){ ?>
		<h5 class="col-md-12 caps-font package-flight-heading">
            <?php if($_GET['status'] == 'arrived'){ ?>
                <div class="custom-control custom-checkbox float-left">
                    <input type="checkbox" class="custom-control-input" name="flightID" value="<?php echo $flightID; ?>" id="check-flight-<?php echo $flightID; ?>" data-rooturl="<?php echo ROOT_URL; ?>" onchange="gotoinvoice(this)">
                    <label class="custom-control-label" for="check-flight-<?php echo $flightID; ?>"></label>
                </div>
            <?php } ?>
            რეისი <?php Other::flight_id($flightID); ?>
        </h5>
        <div class="flight-packages-holder masonry-container row">
		<?php foreach($packageData as $key => $item){ ?>
			<div class="package col-md-6 mb-4">
				<div class="package-inner">
					<div class="tracking-number col-md">
						<i class="fas fa-globe-asia"></i> <b>თრექინგ კოდი</b>: <?php echo $item['productOrder']; ?>
						<span class="badge badge-warning">
                            <?php echo $lang['STATUS_'. strtoupper(str_replace('-', '_', $_GET['status']))]; ?>
                        </span>
					</div>
					<div class="package-panel-body" <?php if(empty($item['productName'])){echo 'style="display:none;"';} ?>>
						<hr>
						<?php if(file_exists('assets/images/product_images/'.$item['file']) && !empty($item['file'])){ ?>
                            <div class="product-image col-lg-5 float-left" style="
                                    background: url('<?php echo ROOT_URL; ?>assets/images/product_images/<?php echo $item['file']; ?>') no-repeat center center;
                                    -webkit-background-size: cover;
                                    -moz-background-size: cover;
                                    -o-background-size: cover;
                                    background-size: cover;
                                    ">
                            </div>
						<?php } ?>
						<div class="col-sm"><b>კატეგორია</b>: <?php echo $item['productName']; ?></div>
						<div class="col-sm"><b>რაოდენობა</b>: <?php echo $item['quantity']; ?></div>
						<div class="col-sm"><b>მაღაზია</b>: <?php echo $item['shop']; ?></div>
						<div class="col-sm"><b>თანხა</b>: <?php echo $item['price']; ?> <?php echo $item['currency']; ?></div>
						<div class="clear"></div>
					</div>
                    <div class="declar-form-container" id="declar-form-<?php echo $item['productOrder']; ?>">

                    </div>
                    <hr>
                    <div class="package-footer col-md-12">
	                    <?php if($_GET['status'] != 'obtained' && $_GET['status'] != 'arrived' && $_GET['status'] != ''){ ?>
                            <?php if(empty($item['productName'])){ ?>
                                <span onclick="getDeclarForm(this)" class="btn btn-success btn-sm col-lg-5" id="declar-btn-<?php echo $item['productOrder']; ?>" data-tracking-code="<?php echo $item['productOrder']; ?>" data-rooturl="<?php echo ROOT_URL; ?>">
                                <i class="fas fa-signature"></i> დეკლარირება
                            </span>
                            <?php } ?>
	                    <?php } ?>
                    <span onclick="triggerSubmit(this)" class="btn btn-success btn-sm col-lg-5" data-rooturl="<?php echo ROOT_URL; ?>" data-packageid="<?php echo $item['productOrder']; ?>" id="save-<?php echo $item['productOrder']; ?>" style="display:none;">
                         <i class="fas fa-save"></i> დამახსოვრება
                    </span>
                    <span class="btn btn-warning btn-sm col-lg-4" data-tracking-code="<?php echo $item['productOrder']; ?>" id="cancel-<?php echo $item['productOrder']; ?>" onclick="cancelDeclar(this)" style="display:none;">
                        <i class="fas fa-window-close"></i> გაუქმება
                    </span>
                        <div class="clear"></div>
                    </div>
				</div>
			</div>
		<?php } ?>
        </div>
	<?php } ?>
</div>

<?php if($_GET['status'] == 'arrived'){ ?>
    </form>
<?php } ?>