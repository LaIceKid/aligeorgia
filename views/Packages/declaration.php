<form action="<?php echo ROOT_URL; ?>updatepackage/" method="post" enctype="multipart/form-data" class="add-item-form declar-item-form" id="declaration-form-<?php echo $_GET['subroute']; ?>">
	<div class="form-row">
	    <input type="hidden" name="dateTime" value="<?php echo date("Y-m-d"); ?>">
        <input type="hidden" class="form-control" id="productOrder" name="productOrder" value="<?php echo $_GET['subroute']; ?>" placeholder="თრექინგ კოდი" required>
		<div class="col-md-12 mb-3">
			<label  class="hidden-label" for="name"></label>
			<select name="name" class="form-control" id="name" required>
				<option value="">საქონლის დასახელება</option>
				<option value="სხვადასხვა ელექტრონული მოწყობილებები">სხვადასხვა ელექტრონული მოწყობილებები</option><option value="ჩანთები და ჩასადებები" selected>ჩანთები და ჩასადებები</option><option value="ფეხსაცმელი">ფეხსაცმელი</option><option value="ტელეფონი და ქსელური მოწყობილობები">ტელეფონი და ქსელური მოწყობილობები</option><option value="ტანსაცმელი, ყველა ტიპის სამოსი">ტანსაცმელი, ყველა ტიპის სამოსი</option><option value="საკვები დანამატები">საკვები დანამატები</option><option value="სათამაშოები და სპორტული ინვენტარი">სათამაშოები და სპორტული ინვენტარი</option><option value="საათები">საათები</option><option value="პარფიუმერია და კოსმეტიკა">პარფიუმერია და კოსმეტიკა</option><option value="ოპტიკური და ფოტო აპარატურა">ოპტიკური და ფოტო აპარატურა</option><option value="ნაბეჭდი პროდუქცია, წიგნები, ბროშურები">ნაბეჭდი პროდუქცია, წიგნები, ბროშურები</option><option value="მუსიკალური ინსტრუმენტები და მათი ნაწილები">მუსიკალური ინსტრუმენტები და მათი ნაწილები</option><option value="მინის ნაწარმი">მინის ნაწარმი</option><option value="მედიკამენტები">მედიკამენტები</option><option value="კომპიუტერი, ლეპტოპი და მათი ნაწილები">კომპიუტერი, ლეპტოპი და მათი ნაწილები</option><option value="იარაღები და ხელის ინსტრუმენტები">იარაღები და ხელის ინსტრუმენტები</option><option value="განათება, ჭაღები, ლამპები, ფარები">განათება, ჭაღები, ლამპები, ფარები</option><option value="ბიჟუტერია">ბიჟუტერია</option><option value="ავტო ნაწილები">ავტო ნაწილები</option><option value="სხვადასხვა მზა ნაწარმი">სხვადასხვა მზა ნაწარმი</option>						</select>
		</div>
		<div class="col-md-12 mb-3">
			<label  class="hidden-label" for="quantity"></label>
			<div class="input-group">
				<input type="text" class="form-control" name="quantity" id="quantity" value="1" placeholder="რაოდენობა" required>
			</div>
		</div>
		<div class="col-md-12 mb-3">
			<label  class="hidden-label" for="shop"></label>
			<select name="shop" class="form-control" id="shop" required>
				<option value="" disabled="" >მაღაზია</option>
				<option value="TAOBAO.COM" selected="">TAOBAO.COM</option>
				<option value="EBAY.COM">EBAY.COM</option>
				<option value="ALIEXPRESS.COM">ALIEXPRESS.COM</option>
				<option value="ALIBABA.COM">ALIBABA.COM</option>
				<option value="TMALL.COM">TMALL.COM</option>
				<option value="other">სხვა მაღაზია</option>
			</select>
		</div>

		<div class="col-md-12 mb-3">
			<label  class="hidden-label" for="price"></label>
			<input type="text" class="form-control" name="price" id="price" value="1" placeholder="თანხა" required>
		</div>
		<div class="col-md-12 mb-3">
			<label  class="hidden-label" for="currency"></label>
			<select name="currency" class="form-control" id="currency" required>
				<option value="">ვალუტა</option>
				<option value="CNY" selected>CNY</option>
				<option value="GEL">GEL</option>
				<option value="USD">USD</option>
			</select>
		</div>
        <div class="col-md-5">
            <div class="container-fluid" id="selected-image-container-<?php echo $_GET['subroute']; ?>" style="
                    height:100px;
                    background: url('<?php echo ROOT_URL; ?>assets/images/other/no-default-thumbnail.jpg') no-repeat center center;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover;
                    "
            ></div>
        </div>
		<div class="col-md-7" style="position:relative">
			<label for="fileSelect" class="file-select-label"></label>
			<label class="btn btn-info btn-file file-select">
				<i class="fa fa-file-o"></i> ფაილის არჩევა <input id="fileSelect" type="file" name="files[]" onchange="readURL(this)" style="display: none;" data-productid="<?php echo $_GET['subroute']; ?>">
			</label>
		</div>

		<div class="col-md-12">
			<label for="product-comment">კომენტარი <span class="text-muted">(კომპანია რაღაცას აკეთებს)</span></label>
			<textarea class="form-control" name="" id="product-comment" rows="3"></textarea>
		</div>
	</div>
    <input type="submit" name="submit" value="submit" onclick="updatePackage(this)" id="submit-button-<?php echo $_GET['subroute']; ?>" data-rooturl="<?php echo ROOT_URL; ?>" data-packageid="<?php echo $_GET['subroute']; ?>" style="display: none;">
</form>