<div class="message-place"></div>
<div class="package-main-container masonry-container row">
	<?php foreach($viewmodel as $key => $item){ ?>
		<div class="package col-md-6 mb-4" id="package-outer-<?php echo $item['productOrder']; ?>">
			<div class="package-inner" id="package-inner-<?php echo $item['productOrder']; ?>">
				<div class="tracking-number col-md">
					<b>თრექინგ კოდი</b>: <?php echo $item['productOrder']; ?>
					<span class="badge badge-secondary">
                        <?php echo $lang['STATUS_'. strtoupper(str_replace('-', '_', $_GET['status']))]; ?>
                    </span>
				</div>
				<div class="package-panel-body" <?php if(empty($item['productName'])){echo 'style="display:none;"';} ?>>
					<hr>
					<?php if(file_exists('assets/images/product_images/'.$item['file']) && !empty($item['file'])){ ?>
						<div class="product-image col-lg-5 float-left" style="
                            background: url('<?php echo ROOT_URL; ?>assets/images/product_images/<?php echo $item['file']; ?>') no-repeat center center;
                            -webkit-background-size: cover;
                            -moz-background-size: cover;
                            -o-background-size: cover;
                            background-size: cover;
                        ">
						</div>
					<?php } ?>
					<div class="col-sm"><b>კატეგორია</b>: <?php echo $item['productName']; ?></div>
					<div class="col-sm"><b>რაოდენობა</b>: <?php echo $item['quantity']; ?></div>
					<div class="col-sm"><b>მაღაზია</b>: <?php echo $item['shop']; ?></div>
					<div class="col-sm"><b>თანხა</b>: <?php echo $item['price']; ?> <?php echo $item['currency']; ?></div>
                    <div class="col-sm"><b>კომენტარი:</b><br><?php echo $item['comment']['text']; ?></div>
					<div class="clear"></div>
				</div>
                <div class="declar-form-container" id="declar-form-<?php echo $item['productOrder']; ?>">
                    <!-- HERE GOES FORM-->
                </div>
				<hr>
				<div class="package-footer container">
					<?php if($_GET['status'] != 'obtained' && $_GET['status'] != 'arrived' && $_GET['status'] != ''){ ?>
                        <?php if(empty($item['productName'])){ ?>
                            <span onclick="getDeclarForm(this)" class="btn btn-success btn-sm col-lg-5" id="declar-btn-<?php echo $item['productOrder']; ?>" data-tracking-code="<?php echo $item['productOrder']; ?>" data-rooturl="<?php echo ROOT_URL; ?>">
                                <i class="fas fa-signature"></i> დეკლარირება
                            </span>
                        <?php } ?>
					<?php } ?>
                    <span onclick="triggerSubmit(this)" class="btn btn-success btn-sm col-lg-5" data-rooturl="<?php echo ROOT_URL; ?>" data-packageid="<?php echo $item['productOrder']; ?>" id="save-<?php echo $item['productOrder']; ?>" style="display:none;">
                        <i class="fas fa-save"></i> დამახსოვრება
                    </span>
                    <span class="btn btn-warning btn-sm col-lg-4" data-tracking-code="<?php echo $item['productOrder']; ?>" id="cancel-<?php echo $item['productOrder']; ?>" onclick="cancelDeclar(this)" style="display:none;">
                        <i class="fas fa-window-close"></i> გაუქმება
                    </span>

                    <?php if($_GET['status'] == ''){ ?>
                    <span class="btn btn-danger btn-sm col-lg-1">
                        <i class="fas fa-trash"></i>
                    </span>
                    <span class="btn btn-warning btn-sm col-lg-1">
                        <i class="fas fa-pencil-alt"></i>
                    </span>
                    <?php } ?>
                </div>
			</div>
		</div>
	<?php } ?>
</div>
