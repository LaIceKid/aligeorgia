
<br><br>
<pre>
<?php print_r($_POST); ?>
<?php print_r($_FILES); ?>
</pre>
<h3 class="caps">რეგისტრაცია</h3>

<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="<?php echo ROOT_URL; ?>register/physicalperson/" class="nav-link caps <?php echo isset($_GET['subroute']) && $_GET['subroute'] == 'physicalperson'?'active':''; ?>">
            ფიზიკური პირი
        </a>
    </li>
    <li class="nav-item">
        <a href="<?php echo ROOT_URL; ?>register/legalentity/" class="nav-link caps <?php echo isset($_GET['subroute']) && $_GET['subroute'] == 'legalentity'?'active':''; ?>">
            იურიდიული პირი
        </a>
    </li>
</ul>
<?php
    $postdata = http_build_query(
        $_POST
    );
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-Type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );
    $context  = stream_context_create($opts);
?>
<div class="tab-content" id="myTabContent">
    <div class="border-1 padding-custom-15">
        <?php if(!empty($_GET['subroute'])){ ?>
            <?php echo file_get_contents(ROOT_URL.$_GET['subroute'],  false, $context); ?>
        <?php } ?>
    </div>
</div>
