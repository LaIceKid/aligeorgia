<?php
    $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
    $UserModel = new UserModel();
    $viewmodel = $UserModel->invoice($_GET['id'], $_SESSION['user_data']['id']);

if(empty($viewmodel)){
        header('location:'.ROOT_URL);
        exit();
    }
    $dateIn = date("d/m/Y");
?>

<?php
    if(isset($_POST['pay'])){
        header('location:'.ROOT_URL);
        $UserModel->pay();
    }
?>

<script>
    $("document").ready(function(){
        $("#invoice-pay-button-fake").hide();
        $("#invoice-form").on("submit", function(){
            $("#invoice-pay-button").hide();
            $("#invoice-pay-button-fake").show();
        });
    });
</script>


<form id="invoice-form" action="<?php echo ROOT_URL; ?>account/pay-invoice/<?php echo $_GET['id'] ?>" method="post" style="display:inline-block;">
    <input type="submit" class="btn btn-warning" id="invoice-pay-button" value="გადახდა" name="pay" <?php if($viewmodel[0]['state'] == 'paid'){echo 'disabled';} ?> />
    <input type="submit" class="btn btn-warning" id="invoice-pay-button-fake" style="display:none;" value="გადახდა" name="pay" disabled />
</form>

<input name="b_print" type="button" class="ipt btn btn-info" onClick="printdiv('div_print');" value="ინვოისის ბეჭდვა">

<hr>

<div id="div_print">
	<h3 style="text-align:center;">ანგარიშ-ფაქტურა (ინვოისი)</h3>
	<br />
	<br />
	<table class="table invoiceTable" style="width:100%">
		<tr>
			<td style="width:50%">
				შპს  ალიჯორჯია	<br />
				ს/კ: 402011568<br />
				ფაქტობრივი მისამართი<br />
				ქ. თბილისი უ. ჩხეიძის 19<br />
			</td>
			<td>
				თარიღი: <?php echo $dateIn; ?><br />
				ნომერი: <?php $UserModel->invNumb($_SESSION['user_data']['id'], $_GET['id']); ?><br />
			</td>
		</tr>
		<tr>
			<td>
				სს. "საქართველოს ბანკი"<br />
				ანგ: № GE75BG0000000917631100<br />
				ბანკის კოდი 220101502 / BAGAGE22 /<br />
				დანიშნულება<br />
				ტრანსპორტირების საფასური<br />
			</td>
			<td>
				<img class="invoiceLogoImg" src="<?php echo ROOT_URL; ?>assets/images/logo.png" width="200" />
			</td>
		</tr>
	</table>

	<h4>მიმღები</h4>

	<table class="table invoisTable" style="width:600px">
		<tr>
			<td style="width:50%">
				სახელი, გვარი:
			</td>
			<td>
				<?php echo $viewmodel[0]['name']; ?>
			</td>
		</tr>
		<tr>
			<td>
				პირადი ნომერი
			</td>
			<td>
				<?php echo $viewmodel[0]['passport']; ?>
			</td>
		</tr>
	</table>

	<table class="table table-bordered invoiceProductDataTable">
		<tr class="active">
			<th>
				გზავნილის ნომერი
			</th>
			<th>
				რაოდენობა
			</th>
			<th>
				წონა (კგ)
			</th>
			<th>
				ტარიფი	(USD)
			</th>
			<th>
				<div style="display:inline-block;">ეროვნული ბანკის კურსი</div>
			</th>
			<th>
				თანხა (ლარი)
			</th>
		</tr>
		<?php $totalPrice = 0; ?>
		<?php $totalWeight = 0; ?>
		<?php foreach($viewmodel as $item): ?>
			<tr>
				<td>
					<?php echo $item['productOrder']; ?>
				</td>
				<td>

				</td>
				<td>
					<?php echo $item['weight']; ?>
				</td>
				<td>
					<?php echo $item['tariff']; ?>
				</td>
				<td>
					<?php echo $currency = $item['currentUSDrate']; ?>
				</td>
				<td>
					<?php $itemPrice = round(($item['weight'] * $item['tariff']) * $currency, 2); echo number_format((float)$itemPrice, 2, '.', ''); ?>
				</td>
			</tr>
			<?php $totalPrice += $itemPrice; ?>
			<?php $totalWeight += $item['weight']; ?>
			<?php $prodIDs[] = $item['prodID']; ?>
		<?php endforeach; ?>
		<?php $_SESSION['pay']['product'] = implode(',', $prodIDs); ?>
		<tr>
			<td>
				<b>სულ</b>
			</td>
			<td>
				<?php echo count($viewmodel); ?>
			</td>
			<td>
				<?php echo $totalWeight; ?>
			</td>
			<td>

			</td>
			<td>

			</td>
			<td>
				<b><?php echo number_format((float)$totalPrice, 2, '.', ''); $_SESSION['pay']['totalPrice'] = $totalPrice; ?></b>
			</td>
		</tr>
	</table>

	<div>
		კონტაქტი:	<br />
		ვებ გვერდი: www.AliGeorgia.Ge	<br />
		ელ.ფოსტა:    info.aligeorgia@gmail.com		<br />
		ტელეფონი:  (+032) 2 196 191		<br />

		გმადლობთ, რომ სარგებლობთ ჩვენი მომსახურებით!			<br />
	</div>
</div>