<?php $PackagesModel = new PackagesModel; ?>
<h5>ამანათის დამატება</h5>
<?php $PackagesModel->add_item(); ?>
<br>


<form action="<?php echo ROOT_URL; ?>insertpackage/" method="post" enctype="multipart/form-data" class="add-item-form needs-validation" novalidate>
    <input type="hidden" name="dateTime" value="<?php echo date("Y-m-d"); ?>">
    <div class="form-row">
        <div class="col-md mb-3">
            <label for="productOrder">თრექინგ კოდი</label>
            <input type="text" class="form-control" id="productOrder" name="productOrder" value="" required>
        </div>
        <div class="col-md mb-3">
            <label for="name">საქონლის დასახელება</label>
            <select name="name" class="form-control" id="name" required>
                <option value="">--</option>
                <option value="სხვადასხვა ელექტრონული მოწყობილებები">სხვადასხვა ელექტრონული მოწყობილებები</option><option value="ჩანთები და ჩასადებები">ჩანთები და ჩასადებები</option><option value="ფეხსაცმელი">ფეხსაცმელი</option><option value="ტელეფონი და ქსელური მოწყობილობები">ტელეფონი და ქსელური მოწყობილობები</option><option value="ტანსაცმელი, ყველა ტიპის სამოსი">ტანსაცმელი, ყველა ტიპის სამოსი</option><option value="საკვები დანამატები">საკვები დანამატები</option><option value="სათამაშოები და სპორტული ინვენტარი">სათამაშოები და სპორტული ინვენტარი</option><option value="საათები">საათები</option><option value="პარფიუმერია და კოსმეტიკა">პარფიუმერია და კოსმეტიკა</option><option value="ოპტიკური და ფოტო აპარატურა">ოპტიკური და ფოტო აპარატურა</option><option value="ნაბეჭდი პროდუქცია, წიგნები, ბროშურები">ნაბეჭდი პროდუქცია, წიგნები, ბროშურები</option><option value="მუსიკალური ინსტრუმენტები და მათი ნაწილები">მუსიკალური ინსტრუმენტები და მათი ნაწილები</option><option value="მინის ნაწარმი">მინის ნაწარმი</option><option value="მედიკამენტები">მედიკამენტები</option><option value="კომპიუტერი, ლეპტოპი და მათი ნაწილები">კომპიუტერი, ლეპტოპი და მათი ნაწილები</option><option value="იარაღები და ხელის ინსტრუმენტები">იარაღები და ხელის ინსტრუმენტები</option><option value="განათება, ჭაღები, ლამპები, ფარები">განათება, ჭაღები, ლამპები, ფარები</option><option value="ბიჟუტერია">ბიჟუტერია</option><option value="ავტო ნაწილები">ავტო ნაწილები</option><option value="სხვადასხვა მზა ნაწარმი">სხვადასხვა მზა ნაწარმი</option>						</select>
        </div>
        <div class="col-md-2 mb-3">
            <label for="quantity">რაოდენობა</label>
            <div class="input-group">
                <input type="text" class="form-control" name="quantity" id="quantity" required>
            </div>
        </div>
        <div class="col-md-2 mb-3">
            <label for="shop">მაღაზია</label>
            <select name="shop" class="form-control" id="shop" required>
                <option value="" disabled="" selected="">--</option>
                <option value="TAOBAO.COM">TAOBAO.COM</option>
                <option value="EBAY.COM">EBAY.COM</option>
                <option value="ALIEXPRESS.COM">ALIEXPRESS.COM</option>
                <option value="ALIBABA.COM">ALIBABA.COM</option>
                <option value="TMALL.COM">TMALL.COM</option>
                <option value="other">სხვა მაღაზია</option>
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md mb-3">
            <label for="price">თანხა</label>
            <input type="text" class="form-control" name="price" id="price" required>
        </div>
        <div class="col-md mb-3">
            <label for="currency">ვალუტა</label>
            <select name="currency" class="form-control" id="currency" required>
                <option value="">--</option>
                <option value="CNY">CNY</option>
                <option value="GEL">GEL</option>
                <option value="USD">USD</option>
            </select>
        </div>
        <div class="col-md-2">
            <div class="container-fluid" id="selected-image-container-none" style="
                    height:100px;
                    background: url('<?php echo ROOT_URL; ?>assets/images/other/no-default-thumbnail.jpg') no-repeat center center;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover;
                    "
            ></div>
        </div>
        <div class="col-md-2 mb-3">
            <label for="fileSelect">&nbsp;</label><br>
            <label class="btn btn-info btn-file file-select" style="width: 100%;">
                <i class="fa fa-file-o"></i> ფაილის არჩევა <input id="fileSelect" type="file" name="files[]" onchange="readURL(this)" style="display: none;" data-productid="none">
            </label>
        </div>

        <div class="col-md-12 mb-3">
            <label for="product-comment">კომენტარი <span class="text-muted">(კომპანია რაღაცას აკეთებს)</span></label><br>
            <textarea class="form-control" name="" id="product-comment" rows="5"></textarea>
        </div>
    </div>
    <button class="btn btn-success" type="submit">დამატება</button>

</form>
