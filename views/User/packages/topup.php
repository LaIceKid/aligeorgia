<pre>
<?php print_r($_REQUEST); ?>
</pre>

<div class="container">
	<form action="<?php echo ROOT_URL; ?>topup"
	      method="post"
	      class="balance-form row">
		<div class="col-md-12">
	    <h3>
	        ბალანსის
	        შევსება</h3>
	    <div class="alert alert-warning">ბალანსის შესავსებად შეიყვანეთ სასურველი თანხა ქვემოთ მოცემულ ველში და დააჭირეთ ღილაკს "გადახდა".</div>
		</div>
	    <div class="form-group col-md-6">
	        <label for="usr">ლარი:</label>
	        <input type="number"
	               name="lari"
	               placeholder="0"
	               class="form-control"
	               />
	    </div>
	    <div class="form-group col-md-6">
	        <label for="usr">თეთრი:</label><br/>
	        <input type="number"
	               name="tetri"
	               placeholder="00"
	               class="form-control"
	               />
	    </div>

	    <div class="form-group col-md-1">
	        <input type="submit"
	               name="submit"
	               value="გადახდა"
	               class="btn btn-success"/>
	    </div>
	</form>
</div>