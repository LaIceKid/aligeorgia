<?php $Other = new Other; ?>
<div class="row account">
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="profile-image float-left" style="
          background: url('<?php echo ROOT_URL; ?>assets/images/avatar/<?php echo $_SESSION['user_data']['avatar']; ?>') no-repeat center center;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        ">
        </div>
        <h5 class="card-title"><?php echo $_SESSION['user_data']['name']; ?></h5>
          <p class="card-text">
              AX კოდი: <?php echo Other::account_id($_SESSION['user_data']['id']); ?>
          </p>
          <p class="card-text">
              ოფისი: <?php echo $lang[strtoupper($_SESSION['user_data']['office'].'_OFFICE')]; ?>
              <a href=""><i class="fas fa-pencil-alt"></i></a>
          </p>
      </div>
      <ul class="list-group list-group-flush user-menu-items">
          <a href="<?php echo ROOT_URL; ?>account/add-item/">
              <li class="list-group-item <?php echo $_GET['subroute'] == 'add-item'?'user-menu-background':''; ?>">
                  <i class="fas fa-plus text-success"></i> ამანათის დამატება
              </li>
          </a>

          <a href="<?php echo ROOT_URL; ?>account/awaiting/">
          <li class="list-group-item <?php echo $_GET['subroute'] == 'awaiting'?'user-menu-background':''; ?>">
            <i class="fas fa-clock text-default"></i> მოლოდინის რეჟიმი
          </li>
        </a>
        <a href="<?php echo ROOT_URL; ?>account/warehouse/">
          <li class="list-group-item <?php echo $_GET['subroute'] == 'warehouse'?'user-menu-background':''; ?>">
            <i class="fas fa-warehouse text-info"></i> საწყობში
          </li>
        </a>
        <a href="<?php echo ROOT_URL; ?>account/pending/">
          <li class="list-group-item <?php echo $_GET['subroute'] == 'pending'?'user-menu-background':''; ?>">
            <i class="fas fa-plane-departure text-warning"></i> გამოგზავნილი
          </li>
        </a>
        <a href="<?php echo ROOT_URL; ?>account/arrived/">
          <li class="list-group-item <?php echo $_GET['subroute'] == 'arrived'?'user-menu-background':''; ?>">
            <i class="fas fa-plane-arrival text-primary"></i> ჩამოსული
          </li>
        </a>
        <a href="<?php echo ROOT_URL; ?>account/obtained/">
          <li class="list-group-item <?php echo $_GET['subroute'] == 'obtained'?'user-menu-background':''; ?>">
            <i class="fas fa-hands-helping text-success"></i> მიღებული
          </li>
        </a>


        <a href="<?php echo ROOT_URL; ?>account/topup/">
          <li class="list-group-item <?php echo $_GET['subroute'] == 'topup'?'user-menu-background':''; ?>">
            <i class="fas fa-wallet text-muted"></i> ბალანსის შევსება
          </li>
        </a>
        <a href="<?php echo ROOT_URL; ?>account/settings/">
          <li class="list-group-item <?php echo $_GET['subroute'] == 'settings'?'user-menu-background':''; ?>">
            <i class="fas fa-user-cog text-muted"></i> პარამეტრები
          </li>
        </a>
        <a href="<?php echo ROOT_URL; ?>account/messages/">
          <li class="list-group-item <?php echo $_GET['subroute'] == 'messages'?'user-menu-background':''; ?>">
            <i class="fas fa-comments text-muted"></i> შეტყობინებები
          </li>
        </a>
      </ul>
      <div class="card-body row">
        <div class="balance col-md-6 text-center text-lg-left">
	        <?php $UserModel = new UserModel(); ?>
            ბალანსი: ₾ <?php echo $UserModel->balanceView(); ?>
          <br>
          <small class="text-muted text-small">
<!--            ბოლო შევსება: 24/02/2019-->
          </small>
        </div>
        <div class="col-md-6 text-center text-lg-right">
          <a href="<?php echo ROOT_URL; ?>logout/" class="btn btn-secondary btn-sm btn-logout">გამოსვლა</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-8">
    <?php
      if(file_exists(dirname(__FILE__).'/packages/'.$_GET['subroute'].'.php')){
        include(dirname(__FILE__).'/packages/'.$_GET['subroute'].'.php');
      }else{
        include(dirname(__FILE__).'/packages/awaiting.php');
      }
    ?>
  </div>
</div>
