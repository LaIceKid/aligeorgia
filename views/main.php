<?php
  //Get Language
  $LanguageModel = new LanguageModel;
  $lang = $LanguageModel->SelectLanguage();
  //Get Currency
  $CurrencyModel = new CurrencyModel;
  $Other = new Other;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>AliGeorgia.ge</title>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="Green Man" name="description">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.min.css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <link href="<?php echo ROOT_URL; ?>assets/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<!--  <div class="test-div"  >-->
<!--  	<pre class="test-pre">-->
<!--      --><?php ////print_r($_SESSION); ?>
<!--		</pre>-->
<!--  </div>-->

  <nav class="navbar navbar-expand-lg navbar-light white main-navbar">
    <div class="container">
      <a class="navbar-brand" href="#">
        <img src="<?php echo ROOT_URL; ?>assets/images/logo.png" width="100" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>">მთავარი <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>about/">ჩვენს შესახებ</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>shops/">ონლაინ მაღაზიები</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>schedule/">რეისების განრიგი</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>contact/">კონტაქტი</a>
          </li>
        </ul>
        <ul class="navbar-nav mr-right">
          <?php if(isset($_SESSION['user_data'])){ ?>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle profile-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="profile-image-small float-left" style="
                  background: url('<?php echo ROOT_URL; ?>assets/images/avatar/<?php echo $_SESSION['user_data']['avatar']; ?>') no-repeat center center;
                  -webkit-background-size: cover;
                  -moz-background-size: cover;
                  -o-background-size: cover;
                  background-size: cover;
                ">
                </div>
                <?php echo $_SESSION['user_data']['name']; ?> |
                <?php echo $Other->account_id($_SESSION['user_data']['id']); ?>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <div class="dropdown-item balance-view">
                    <?php $UserModel = new UserModel(); ?>
                  ბალანსი: ₾ <?php echo $UserModel->balanceView(); ?>
                  <br>
                  <small class="text-muted text-small">
<!--                    ბოლო შევსება: 24/02/2019-->
                  </small>
                </div>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?php echo ROOT_URL; ?>account/topup/"><i class="fas fa-wallet text-muted"></i> ბალანსის შევსება</a>
                <a class="dropdown-item" href="<?php echo ROOT_URL; ?>account/settings/"><i class="fas fa-user-cog text-muted"></i> პარამეტრები</a>
                <a class="dropdown-item" href="<?php echo ROOT_URL; ?>account/messages/"><i class="fas fa-comments text-muted"></i> შეტყობინებები</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?php echo ROOT_URL; ?>logout/"><i class="fas fa-power-off"></i> გამოსვლა</a>
              </div>
            </li>
          <?php }else{ ?>
            <li class="login-register-top-menu-btn nav-item dropdown">
              <a class="nav-link dropdown-toggle profile-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                ავტორიზაცია
                <span class="text-muted">/</span>
                რეგისტრაცია
              </a>
              <div class="dropdown-menu dropdown-menu-right login-register-top-menu-content" aria-labelledby="navbarDropdown">
                <form action="<?php echo ROOT_URL; ?>login/" method="post" class="text-center border login p-5">
                <p class="h4 mb-4">ავტორიზაცია</p>

                <!-- Email -->
                <input type="text" name="axcode" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="AX კოდი">

                <!-- Password -->
                <input type="password" name="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="პაროლი">

                <div class="d-flex justify-content-around">
                    <div class="remember-me">
                        <!-- Remember me -->
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                            <label class="custom-control-label" for="defaultLoginFormRemember">დამიმახსოვრე</label>
                        </div>
                    </div>
                    <div>
                        <!-- Forgot password -->
                        <a href=""><i class="fas fa-key"></i> აღდგენა</a>
                    </div>
                </div>

                <!-- Sign in button -->
                <button class="btn btn-info btn-block my-4" type="submit"><i class="fas fa-sign-in-alt"></i> ავტორიზაცია</button>

                <!-- Register -->
                <p>
                    <a href="<?php echo ROOT_URL; ?>register/physicalperson"><i class="fas fa-user-plus"></i> რეგისტრაცია</a>
                </p>

                <!-- Social login -->
                <!-- <p>ან ავტორიზაცია:</p>

                <a type="button" class="light-blue-text mx-2">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a type="button" class="light-blue-text mx-2">
                    <i class="fab fa-twitter"></i>
                </a>
                <a type="button" class="light-blue-text mx-2">
                    <i class="fab fa-linkedin-in"></i>
                </a>
                <a type="button" class="light-blue-text mx-2">
                    <i class="fab fa-github"></i>
                </a> -->

                </form>

              <!-- <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">New around here? Sign up</a>
              <a class="dropdown-item" href="#">Forgot password?</a> -->
            </div>
            </li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </nav>

  <main>
    <div class="container">
      <?php Messages::display(); ?>
      <?php require($view); ?>
    </div>
  </main>


      <br><br><br><br>
      <!-- Footer -->
      <footer class="page-footer font-small unique-color-dark pt-4">

          <!-- Footer Elements -->
          <div class="container">

            <!-- Grid row-->
            <div class="row">

              <!-- Grid column -->
              <div class="col-md-12 py-5">
                <div class="mb-5 text-center">
                  <!-- Facebook -->
                  <a class="fb-ic">
                    <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                  </a>
                  <!-- Twitter -->
                  <a class="tw-ic">
                    <i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                  </a>
                  <!-- Google +-->
                  <a class="gplus-ic">
                    <i class="fab fa-google-plus-g fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                  </a>
                  <!--Linkedin -->
                  <a class="li-ic">
                    <i class="fab fa-linkedin-in fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                  </a>
                  <!--Instagram-->
                  <a class="ins-ic">
                    <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                  </a>
                  <!--Pinterest-->
                  <a class="pin-ic">
                    <i class="fab fa-pinterest fa-lg white-text fa-2x"> </i>
                  </a>
                </div>
              </div>
              <!-- Grid column -->

            </div>
            <!-- Grid row-->

          </div>
          <!-- Footer Elements -->

          <!-- Copyright -->
          <div class="footer-copyright text-center py-3">© 2019 ყველა უფლება დაცულია:
            <a href="https://mdbootstrap.com/education/bootstrap/"> AliGeorgia.ge</a>
          </div>
          <!-- Copyright -->

        </footer>
        <!-- Footer -->



    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
    <script charset="utf-8" src="<?php echo ROOT_URL; ?>assets/js/geo.js" type="text/javascript"></script>
    <script charset="utf-8" src="<?php echo ROOT_URL; ?>assets/js/main.js" type="text/javascript"></script>
</body>
</html>
