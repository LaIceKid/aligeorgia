<div class="row">
  <div class="col-md-4">
    <form action="<?php echo ROOT_URL; ?>login/" method="post" class="text-center border login p-5">
    <p class="h4 mb-4">ავტორიზაცია</p>

    <!-- Email -->
    <input type="text" name="axcode" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="AX კოდი">

    <!-- Password -->
    <input type="password" name="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="პაროლი">

    <div class="d-flex justify-content-around">
        <div class="remember-me">
            <!-- Remember me -->
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                <label class="custom-control-label" for="defaultLoginFormRemember">დამიმახსოვრე</label>
            </div>
        </div>
        <div>
            <!-- Forgot password -->
            <a href=""><i class="fas fa-key"></i> აღდგენა</a>
        </div>
    </div>

    <!-- Sign in button -->
    <button class="btn btn-info btn-block my-4" type="submit"><i class="fas fa-sign-in-alt"></i> ავტორიზაცია</button>

    <!-- Register -->
    <p>
        <a href="<?php echo ROOT_URL; ?>register/physicalperson"><i class="fas fa-user-plus"></i> რეგისტრაცია</a>
    </p>

    <!-- Social login -->
    <!-- <p>ან ავტორიზაცია:</p>

    <a type="button" class="light-blue-text mx-2">
        <i class="fab fa-facebook-f"></i>
    </a>
    <a type="button" class="light-blue-text mx-2">
        <i class="fab fa-twitter"></i>
    </a>
    <a type="button" class="light-blue-text mx-2">
        <i class="fab fa-linkedin-in"></i>
    </a>
    <a type="button" class="light-blue-text mx-2">
        <i class="fab fa-github"></i>
    </a> -->

    </form>
  </div>

  <div class="col-md-8">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active" style="
          background: url('http://hawthornlogistics.ie/wp-content/uploads/2015/07/Screen-Shot-2015-07-07-at-11.23.53.png') no-repeat center center;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        ">
          <div class="carousel-caption d-none d-md-block">
            <h5>Title</h5>
            <p>Text</p>
          </div>
        </div>
        <div class="carousel-item" style="
          background: url('https://image.freepik.com/free-photo/cheerful-courier-giving-package-client_23-2147767714.jpg') no-repeat center center;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        ">
          <div class="carousel-caption d-none d-md-block">
            <h5>Title</h5>
            <p>Text</p>
          </div>
        </div>
        <div class="carousel-item" style="
          background: url('https://www.apsfulfillment.com/wp-content/uploads/2017/03/APS_28.jpg') no-repeat center center;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        ">
          <div class="carousel-caption d-none d-md-block">
            <h5>Title</h5>
            <p>Text</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>

</div>


<br><br><br><br>


<p class="h4 mb-4 text-center">რეისების განრიგი</p>
<!--Table-->
<div class="table-responsive">
<table class="table table-hover text-center flight-table">
<!--Table head-->
  <thead>
    <tr>
      <th>რეისის ნომერი</th>
      <th>გამოფრენა</th>
      <th>სავარაუდო თარიღი</th>
      <th>ჩამოფრენის თარიღი</th>
    </tr>
  </thead>
  <!--Table head-->
  <!--Table body-->
  <tbody>
    <?php foreach($viewmodel['fromtPageSchedule'] as $sKey => $sItem){ ?>
        <tr>
            <td><?php echo Other::flight_id($sItem['id']); ?></td>
            <td><?php echo $sItem['startDate']; ?></td>
            <td><?php echo $sItem['date']; ?></td>
            <td><?php echo $sItem['endDate']; ?></td>
        </tr>
    <?php } ?>
  </tbody>
  <!--Table body-->
</table>
<!--Table-->
</div>

<br/><br/><br/><br/>

<div class="card-group">

  <div class="card">
    <div class="card-body">
      <h5 class="card-title">მოცულობითი წონის კალკულატორი</h5>
      <hr>
      <form action="" method="post" class="weightForm" data-rooturl="<?php echo ROOT_URL; ?>">
        <div class="aboutProdDiv">
          <div class="col-xs-custom">
            <label for="length">სიგრძე</label>
            <input class="form-control" id="length" name="length" type="number" onkeyup="volumeCalculator(this)">
          </div>
          <div class="col-xs-custom">
            <label for="width">სიგანე</label>
            <input class="form-control" id="width" name="width" type="number" onkeyup="volumeCalculator(this)">
          </div>
          <div class="col-xs-custom">
            <label for="height">სიმაღლე</label>
            <input class="form-control" id="height" name="height" type="number" onkeyup="volumeCalculator(this)">
          </div>
        </div>
        <div class="clear"></div>
        <label class="labelWeight">წონა</label>
        <div class="calcRes"><div class="form-control result" id="value-calculator"></div></div>
      </form>
    </div>
    <div class="card-footer">
      <small class="text-muted">შეიტანეთ მოცულობის პარამეტრები 4x7x9x8</small>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <h5 class="card-title">ვალუტის კონვერტერი</h5>
      <hr>
      <form class="currencyForm" class="currencyForm" data-rooturl="<?php echo ROOT_URL; ?>">
        <div class="curencyDiv">
          <label for="curency">CNY</label>
          <input class="form-control" id="curency" name="curr" type="text" onkeyup="currencyCalculator()">

          <label class="labelWidth">ლარი</label>
          <div class="form-control result2" id="currency-calculator"></div>
        </div>
      </form>
    </div>
    <div class="card-footer">
      <small class="text-muted">ჩინური იენი ლართან მიმართებაში</small>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <h5 class="card-title">ტარიფები</h5>
      <table class="table table-hover tariff-table">
        <thead>
          <th>ტიპი</th>
          <th>ტარიფი</th>
        </thead>
        <tbody>
          <tr>
            <td>ფიზიკური პირი</td>
            <td>$7.5</td>
          </tr>
          <tr>
            <td>იურიდიული პირი</td>
            <td>$7.2</td>
          </tr>
          <tr>
            <td>პირადი გზავნილები</td>
            <td>$7.2</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="card-footer">
      <small class="text-muted">ბოლო განახლება: დღეს</small>
    </div>
  </div>

</div>
