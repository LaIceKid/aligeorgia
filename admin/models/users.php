<?php

class UsersModel extends Model{
	public function all(){
		$this->query("SELECT * FROM users LIMIT 100");
		$rows = $this->resultSet();
		return $rows;
	}

	public function copy(){
		$this->query('SELECT id, name, nameEn, email, address, phone, ban FROM users');
		$rows = $this->resultSet();
		return $rows;
	}

	public function edit(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		if(!isset($get['userid'])){
			return false;
		}
		if(empty($get['userid'])){
			return false;
		}
		$this->query('SELECT * FROM users WHERE id = :id');
		$this->bind('id', $get['userid']);
		$row = $this->single();
		return $row;
	}

	public function update(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		if(!isset($get['userid'])){return false;}
		if(empty($get['userid'])){return false;}
		if(!isset($post)){return false;}
		if(empty($post)){return false;}
		$this->query('UPDATE users SET name = :name, nameEn = :nameEn, email = :email, register_date = :register_date, passport = :passport, birthday = :birthday, phone = :phone, address = :address WHERE id = :id');
		$this->bind('name', $post['name']);
		$this->bind('nameEn', $post['nameEn']);
		$this->bind('email', $post['email']);
		$this->bind('register_date', $post['register_date']);
		$this->bind('passport', $post['passport']);
		$this->bind('birthday', $post['birthday']);
		$this->bind('phone', $post['phone']);
		$this->bind('address', $post['address']);
		$this->bind('id', $get['userid']);

		$this->execute();

		if ($this->rowCount()) {
			return Messages::setMsg('UPDATE_USER_SUCCESS', 'success');
		} else {
			return Messages::setMsg('UPDATE_USER_ERROR', 'error');
		}
	}
}