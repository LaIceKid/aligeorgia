<?php
class MultiModel extends Model{
	public function Index(){
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		if(isset($post['deleteInvoices'])){
			$this->deleteInvoices($post['startPoint'], $post['endPoint']);
		}
		if(isset($post['alter'])){
			echo $this->alterTable($post['alterPoint']);
		}
		if(isset($post['deleteflights'])){
			$this->deleteFlights($post['startPoint'], $post['endPoint']);
		}
		if(isset($post['alterFlights'])){
			echo $this->alterFlightsTable($post['alterPoint']);
		}
	}

	public function deleteInvoices($startPoint, $endPoint){
		$this->query('DELETE FROM invoice WHERE id BETWEEN :startPoint AND :endPoint');
		$this->bind(':startPoint', $startPoint);
		$this->bind(':endPoint', $endPoint);
		$this->execute();
		header('location:'.ROOT_URL.'multi/');
	}

	public function alterTable($alterPoint){
		$this->query('ALTER TABLE invoice AUTO_INCREMENT = '.$alterPoint.'');
		$this->execute();
		header('location:'.ROOT_URL.'multi/');
	}

	public function deleteFlights($startPoint, $endPoint){
		$this->query('DELETE FROM flight WHERE id BETWEEN :startPoint AND :endPoint');
		$this->bind(':startPoint', $startPoint);
		$this->bind(':endPoint', $endPoint);
		$this->execute();
		header('location:'.ROOT_URL.'multi/');
	}

	public function alterFlightsTable($alterPoint){
		$this->query('ALTER TABLE flight AUTO_INCREMENT = '.$alterPoint.'');
		$this->execute();
		header('location:'.ROOT_URL.'multi/');
	}


	public function csvArray(){
		if(isset($_POST["submit"])){
			if(isset($_FILES["file"])){
				//if there was an error uploading the file
				if($_FILES["file"]["error"] > 0){
					echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
				}else{
					//Print file details
					echo "Upload: " . $_FILES["file"]["name"] . "<br />";
					echo "Type: " . $_FILES["file"]["type"] . "<br />";
					echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
					echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";

					//if file already exists
					if (file_exists("upload/" . $_FILES["file"]["name"])){
						echo $_FILES["file"]["name"] . " already exists. ";
					}else{
						//Store file in directory "upload" with the name of "uploaded_file.txt"
						$fileName = time().'.cvs';
						move_uploaded_file($_FILES["file"]["tmp_name"], "assets/csv/" . $fileName);
						echo "Stored in: " . "upload/" . $_FILES["file"]["name"] . "<br />";
						$csv = array();
						$lines = file("assets/csv/".$fileName, FILE_IGNORE_NEW_LINES);

						foreach ($lines as $key => $value){
							$csv[$key] = str_getcsv($value);
						}

						return $csv;
					}
				}
			}else{
				echo "No file selected <br />";
			}
		}
	}

	public function prodArray(){
		$this->query('SELECT * FROM products WHERE status != "obtained"');
		$rows = $this->resultSet();
		return $rows;
	}
	//მრავალგანზომილებიანი მასივში ძებნა
	public function in_array_r($needle, $haystack, $strict = false) {
		if(!empty($haystack)){
			foreach ($haystack as $item) {
				if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
					return true;
				}
			}
		}
		return false;
	}

	public function csvProcess($csvArray, $prodArray){
		// echo '<pre>';
		// print_r($csvArray);
		// echo '</pre>';
		if(empty($csvArray)){
			return;
		}

		$total = count($csvArray);
		$i=1;

		if(count($csvArray) < 1){
			return;
		}

		foreach($csvArray as $csv){
			$Other = new Other();
			if($this->in_array_r($csv[0], $prodArray)){
				$this->query('SELECT * FROM users WHERE id = "'.$Other->axCodeReverse($csv[2]).'"');
				$row = $this->single();
				if(!empty($row)){
					//Success Msg count
					$success_user['success'][$csv[0]] = $csv[2];
					//Update
					$this->query('UPDATE products SET weight = :weight, status = :status, tariff = :tariff, flightID = :flightID, chinaDate = :chinaDate, arrived_date = :arrived_date, sent_date = :sent_date,  upload_method = :upload_method WHERE productOrder = :productOrder');

					$this->bind(':upload_method', 'list');
					$this->bind(':weight', $csv[1]);
					$this->bind(':productOrder', $csv[0]);
					$this->bind(':status', $csv[3]);
					$this->bind(':tariff', $csv[5]);
					$this->bind(':flightID', $Other->aliCodeReverse($csv[4]));

					if(isset($csv[6]) && !empty($csv[6])){
						$this->bind(':chinaDate', $csv[6]);
					}else{
						$this->bind(':chinaDate', NULL);
					}
					if(isset($csv[7]) && !empty($csv[7])){
						$this->bind(':sent_date', $csv[7]);
					}else{
						$this->bind(':sent_date', NULL);
					}
					if(isset($csv[8]) && !empty($csv[8])){
						$this->bind(':arrived_date', $csv[8]);
					}else{
						$this->bind(':arrived_date', NULL);
					}
				}else{
					$error_user['error'][$csv[0]] = $csv[2];
				}
			}else{
				$this->query('SELECT * FROM users WHERE id = "'.$Other->axCodeReverse($csv[2]).'"');
				$row = $this->single();
				if(!empty($row)){
					//Success Msg count
					$success_user['success'][$csv[0]] = $csv[2];
					//Insert
					$this->query('INSERT INTO products
					(productOrder, weight, userID, status, flightID, tariff, chinaDate, arrived_date, sent_date, buyerName, buyerNameEng, phone, passport, declar, upload_method)
					VALUES
					(:productOrder, :weight, :userID, :status, :flightID, :tariff, :chinaDate, :arrived_date, :sent_date, :buyerName, :buyerNameEng, :phone, :passport, :declar, :upload_method)');

					//FROM CSV FILE
					$this->bind(':upload_method', 'list');
					$this->bind(':productOrder', $csv[0]);
					$this->bind(':weight', $csv[1]);
					$this->bind(':userID', $Other->axCodeReverse($csv[2]));
					$this->bind(':status', $csv[3]);
					$this->bind(':flightID', $Other->aliCodeReverse($csv[4]));
					$this->bind(':tariff', $csv[5]);
					if(isset($csv[6]) && !empty($csv[6])){
						$this->bind(':chinaDate', $csv[6]);
					}else{
						$this->bind(':chinaDate', NULL);
					}
					if(isset($csv[7]) && !empty($csv[7])){
						$this->bind(':sent_date', $csv[7]);
					}else{
						$this->bind(':sent_date', NULL);
					}
					if(isset($csv[8]) && !empty($csv[8])){
						$this->bind(':arrived_date', $csv[8]);
					}else{
						$this->bind(':arrived_date', NULL);
					}

					//STATIC INFO
					$this->bind(':declar', 'admin');

					//FROM USER DB
					$this->bind(':buyerName', $row['name']);
					$this->bind(':buyerNameEng', $row['nameEn']);
					$this->bind(':phone', $row['phone']);
					$this->bind(':passport', $row['passport']);

				}else{
					$error_user['error'][$csv[0]] = $csv[2];
				}
			}
			$this->execute();
		}

		echo '<hr />';
		if(!empty($error_user['error'])){
			?>
			<h3>არ აიტვირთა</h3>
			<table class="table table-bordered" style="width:300px;float:left;">
				<tr>
					<th>თრექინგ კოდი</th>
					<th>AX კოდი</th>
				</tr>
				<?php foreach($error_user['error'] as $tracking => $ax): ?>
					<tr>
						<td><?php echo $tracking; ?></td>
						<td><?php echo $ax; ?></td>
					</tr>
				<?php endforeach; ?>
			</table>
			<div class="clear"></div>
			<?php
			// echo 'Error:<br />';
			// echo '<pre>';

			// print_r($error_user);
			// echo '</pre>';
		}else{
			echo 'პროცესი დასრულდა შეცდომის გარეშე...';
		}
	}
}
