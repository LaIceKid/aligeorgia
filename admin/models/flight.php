<?php
class FlightModel extends Model{
	public function Index(){
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

		if(isset($post['flight_text_save'])){
			$this->query('UPDATE flight_articles SET text = "'.$_POST['text'].'"');
			$this->execute();
			header('location:'.ROOT_URL.'flight/');
		}

		if(isset($post['add'])){
			$this->query('INSERT INTO flight (startDate) VALUES (:startDate)');
			$this->bind(':startDate', $post['startDate']);
			$this->execute();
				//add product
				if($this->lastInsertId()){
					$_SESSION['SuccMsg'] = "თქვენ წარმატებით დაამატეთ პროდუკწია";
					//Redirect
					//header('location:'.ROOT_URL);
				}else{
					echo '-';
				}
		}

		$this->query('SELECT * FROM flight ORDER BY id DESC LIMIT 10');
		$rows = $this->resultSet();
		return $rows;
	}
	
	public function all(){
		$this->query('SELECT * FROM flight ORDER BY id');
		$rows = $this->resultSet();
		return $rows;
	}

	public function flCount(){
		$this->query('SELECT * FROM flight');
		$rows = $this->resultSet();
		return $rows;
	}

	public function flight_articles(){
		$this->query('SELECT * FROM flight_articles');
		$rows = $this->single();
		return $rows;
	}

	public function display(){
		$this->query('SELECT * FROM flight ORDER BY id DESC LIMIT 10');
		$rows = $this->resultSet();
		return $rows;
	}

	public function edit(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		if(isset($_POST['dates'])){
			$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
			$this->query("UPDATE flight SET startDate = :startDate, date = :date, endDate = :endDate WHERE id = :id");
			$this->bind('startDate', $post['firstDate']);
			$this->bind('date', $post['startDate']);
			$this->bind('endDate', $post['endDate']);
			$this->bind('id', $get['subroute']);
			$this->execute();
			$lastid = $this->rowCount();
			if($lastid >= 1){
				Messages::setMsg('UPDATE_SUCCESS', 'success');
			}else{
				Messages::setMsg('UPDATE_ERROR', 'error');
			}

			header('location:'.$_SERVER['HTTP_REFERER']);
			return $lastid;
		}
		$this->query("SELECT * FROM flight WHERE id = :id");
		$this->bind('id', $get['subroute']);

		$rows = $this->resultSet();
		return $rows;

	}

	public function pullPackages($id = 1){
		if(isset($_POST['arrived'])){
			$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
			foreach($post['productID'] as $item){
				print_r($item);
				$this->query("UPDATE products SET status = 'arrived' WHERE id = $item");
				$this->execute();
			}
			return 0;
		}
		$this->query("SELECT * FROM products WHERE flightID = :id ORDER BY userID ");
		$this->bind('id', $id);
		$rows = $this->resultSet();
		return $rows;
	}

	public function packageList(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

		$this->query('SELECT a.*, a.id as prodID, b.* FROM products a
		INNER JOIN users b
		ON a.userID = b.id

		WHERE flightID = "'.$get['id'].'"');
		$rows = $this->resultSet();
		return $rows;

		// $this->query('SELECT * FROM products WHERE flightID = "'.$get['id'].'"');
		// $rows = $this->resultSet();
		// return $rows;
	}

	public function invoiceList(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

		$this->query("
			SELECT i.*, i.id as invID, p.*, u.*, f.* FROM invoice i
			INNER JOIN users u
			ON i.userID = u.id
			INNER JOIN products p
			ON u.id = p.userID
			INNER JOIN flight f
			ON i.flightID = f.id
			WHERE i.flightID = :flightid AND p.flightID = :flightid
		");
		$this->bind('flightid', $get['subroute']);
		$rows = $this->resultSet();
		return $rows;

		// $this->query('SELECT * FROM products WHERE status != "obtained"', MYSQLI_USE_RESULT);
		// $rows = $this->resultSet();
		// return $rows;



		// $this->query('SELECT * FROM products WHERE flightID = "'.$get['id'].'"');
		// $rows = $this->resultSet();
		// return $rows;
	}

	public function invoice(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$this->query('

		SELECT a.*, a.id as prodID, b.*, c.* FROM products a
		INNER JOIN users b
		ON a.userID = b.id
		INNER JOIN flight c
		ON a.flightID = c.id
		WHERE c.id = "'.$get['subroute'].'" GROUP by userID');

		$rows = $this->resultSet();
		return $rows;
	}

	public function invoiceOut($userid = NULL, $flightid = NULL){
		$this->query("
			SELECT p.*, p.id as prodID, u.*, f.*
			FROM products p
			LEFT JOIN users u
			ON p.userID = u.id
			LEFT JOIN flight f
			ON p.flightID = f.id
			WHERE f.id = :flightid AND u.id = :userid GROUP by userID
		");
		$this->bind("flightid", $flightid);
		$this->bind("userid", $userid);
		$rows = $this->resultSet();
		return $rows;
	}

	public function selectProduct($userID, $flightID){
		$this->query('SELECT * FROM products WHERE userID = "'.$userID.'" AND flightID = "'.$flightID.'"');
		$rows = $this->resultSet();
		return $rows;
	}

	public function invNumb($userID, $flightID){
		$this->query('SELECT * FROM invoice WHERE userID = "'.$userID.'" AND flightID = "'.$flightID.'"');
		$row = $this->single();
		echo $row['id'];
	}

	public function delete(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		if(isset($_GET['id'])){
			$this->query('DELETE FROM flight WHERE id = :id');
			$this->bind(':id', $get['id']);
			$this->execute();

			if($this->rowCount() == 1){
				//Redirect
				header('location: '.ROOT_URL.'flight/');
			}else{
				header('location: '.ROOT_URL.'flight/');
			}
		}
	}

	//ამანათების სრული რაოდენობა რეისში
	public function sumPackages($id = ""){
		$this->query('SELECT id FROM products WHERE flightID = :id');
		$this->bind(':id', $id);
		$rows = count($this->resultSet());

		return $rows;
	}

	//ამანათების სრული წონა რეისში
	public function sumWeight($id = ""){
		$this->query('SELECT weight FROM products WHERE flightID = :id');
		$this->bind(':id', $id);
		$rows = $this->resultSet();
		$weight = 0;
		foreach($rows as $item){
			$weight += $item['weight'];
		}
		return $weight;
	}
}
?>
