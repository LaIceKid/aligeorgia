<?php
class PackageModel extends Model{
	public function awaiting(){

	}

	public function transferPackages(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
//		echo '<pre>';
//		print_r($get);
//		print_r($post);
//		echo '</pre>';
//		return;
		$allow_update = true;
		if(!isset($get['status'])){$allow_update = false;}
		if(isset($get['status']) && empty($get['status'])){$allow_update = false;}

		if(!isset($get['success-header'])){$allow_update = false;}
		if(isset($get['success-header']) && empty($get['success-header'])){$allow_update = false;}

		if(!isset($get['error-header'])){$allow_update = false;}
		if(isset($get['error-header']) && empty($get['error-header'])){$allow_update = false;}

		if(!isset($post['productID'])){$allow_update = false;}
		if(isset($post['productID']) && empty($post['productID'])){$allow_update = false;}

		if($allow_update){
			if($this->update_status($post['productID'], $get['status'])){
				header('location:'.ROOT_URL.$get['success-header'].'/');
				if(isset($post['sms-email']) && $post['sms-email'] == 'on'){
					$message = $this->dynamicTexts($get['status']);
					$this->statusMailer('1986', $message['text']);
				}
				if($get['status'] == 'sent'){
					$this->add_flight($post['productID'], $this->insert_flight());
					$this->update_tariff($post['productID'], $post['tariff']);
				}
				if($get['status'] == 'arrived'){
					$this->insert_invoice($post['productID']);
				}
			}
			return;
		}
		Messages::setMsg('TRANSFER_PACKAGES_ERROR_LACK_OF_INFO', 'error');
		header('location:'.ROOT_URL.$get['error-header'].'/');
		return;
	}

	public function insert_invoice($package_ids = array()){
		$products = $this->select_package_by_id($package_ids);
		foreach($products as $pack){
			$this->query('INSERT INTO invoice (flightID, userID) VALUES (:flightID, :userID)');
			$this->bind(':flightID', $pack['flightID']);
			$this->bind(':userID', $pack['userID']);
			$this->execute();
		}
	}

	public function select_package_by_id($package_ids = array()){
		$idz = implode(',', $package_ids);
		$this->query('SELECT * FROM products WHERE id IN ('.$idz.') GROUP BY userID');
		return $this->resultSet();
	}

	public function insert_flight(){
		$this->query("INSERT INTO flight (startDate) VALUES (:date)");
		$this->bind('date', date('Y-m-d'));
		$this->execute();
		return $this->lastInsertId();
	}

	public function add_flight($packages = array(), $flight_id = NULL){
		$update_flight = array();
		foreach($packages as $item) {
			$this->query("UPDATE products SET flightID = :flightID WHERE id = :id");
			$this->bind('flightID', $flight_id);
			$this->bind('id', $item);
			$this->execute();
			$update_flight[] = $this->rowCount();
		}
		if(count($update_flight) == count($packages)){
			return 1;
		}
		return 0;
	}

	public function update_tariff($packages = array(), $tariff = NULL){
		$update_tariff = array();
		foreach($packages as $item) {
			$this->query("UPDATE products SET tariff = :tariff WHERE id = :id");
			$this->bind('tariff', $tariff);
			$this->bind('id', $item);
			$this->execute();
			$update_tariff[] = $this->rowCount();
		}
		if(count($update_tariff) == count($packages)){
			return 1;
		}
		return 0;
	}


	public function update_status($packages = array(), $status = NULL){
		$update_status = array();
		foreach($packages as $item) {
			$this->query("UPDATE products SET status = :status WHERE id = :id");
			$this->bind('status', $status);
			$this->bind('id', $item);
			$this->execute();
			$update_status[] = $this->rowCount();
		}
		if(count($update_status) == count($packages)){
			Messages::setMsg('TRANSFER_PACKAGES_SUCCESS', 'success');
			return 1;
		}
		Messages::setMsg('TRANSFER_PACKAGES_ERROR', 'error');
		return 0;
	}
//	public function update_flight_date($flight_id = NULL, $status = NULL){
//		if($status != 'china-stock'){
//			return 0;
//		}
//		$this->query("UPDATE flight SET endDate = :date WHERE id = :flight_id");
//		$this->bind('date', date('Y-m-d'));
//		$this->bind('flight_id', $flight_id);
//		$this->execute();
//		return $this->rowCount();
//	}

	public function packagedata(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

		if($get['subroute'] == 'list'){
			$upload_method = ' AND upload_method = "list"';
		}elseif($get['subroute'] == 'frontend'){
			$upload_method = ' AND upload_method = ""';
		}else{
			$upload_method = '';
		}

		$limits = 'LIMIT 0, 10';
		if(isset($get['start']) && isset($get['amount'])){
			$limits = 'LIMIT '.$get['start'].', '.$get['amount'].'';
		}

		$status = '';
		if(isset($get['status']) && !empty($get['status'])){
			$status = $get['status'];
			if($get['status'] == 'awaiting'){
				$status = '';
			}
		}

		$this->query('SELECT * FROM products WHERE status = :status '.$upload_method.' '.$limits);
		$this->bind('status', $status);
		$rows = $this->resultSet();

		//BIND OFFICE TO PACKAGE FROM USERS TABLE
		foreach($rows as $key => $value){
			$rows[$key]['office'] = $this->select_user_data($value['userID'])['office'];
		}

		return $rows;
	}

	public function select_user_data($user_id = NULL){
		$this->query("SELECT office FROM users WHERE id = :id");
		$this->bind('id', $user_id);
		return $this->single();
	}

	public function packagedataflight(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$status = 'sent';
		if(isset($get['status']) && $get['status'] == 'giveaway'){
			$status = 'arrived';
		}
		$axcode = '';
		if(isset($get['axcode']) && !empty($get['axcode'])){
			$axcode = "AND userID = '".Other::axCodeReverse(strtoupper($get['axcode']))."'";
		}
		$this->query("SELECT * FROM products WHERE status = :status $axcode");
		$this->bind('status', $status);
		$rows = $this->resultSet();

		//BIND OFFICE TO PACKAGE FROM USERS TABLE
		foreach($rows as $key => $value){
			$rows[$key]['office'] = $this->select_user_data($value['userID'])['office'];
		}

		$result = array();
		foreach($rows as $item){
			$result[$item['flightID']][] = $item;
		}
		return $result;
	}

	//Waiting რეჟიმში მომხმარებლისა და ადმინისტრატორის მთვლელები ცალკ ცალკე...
	public function BackFrontCounter($method = ""){
		$this->query("SELECT upload_method, status FROM products WHERE upload_method = :userId AND status = ''");
		$this->bind(':userId', $method);
		$rows = $this->resultSet();
		return count($rows);
	}

	public function count_packages($status = NULL){
		$this->query("SELECT id FROM products WHERE status = :status");
		$this->bind('status', $status);
		return count($this->resultSet());
	}


	public function warehouse(){

	}

	public function pending(){

	}

	public function arrived(){

	}

	public function obtained(){

	}

	public function giveaway(){

	}

	public function sorting(){
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

		if(isset($post['submitBarcode'])){

			$this->query("SELECT * FROM products WHERE productOrder = :productOrder");
			$this->bind('productOrder', $post['code']);
			$row = $this->resultSet();
			return $row;
		}

		if(isset($post['save'])){
			// echo '<pre>';
			// print_r($post);
			// echo '</pre>';
			$this->query('UPDATE products SET productOrder = :productOrder, productName = :productName, buyerName = :buyerName, userID = :userID, phone = :phone, passport = :passport, shop = :shop, quantity = :quantity, price = :price , weight = :weight, flightID = :flightID, barcode = 1, sorting = 1 WHERE id = :id');


			$this->bind('productOrder', $post['productOrder']);
			$this->bind('productName', $post['productName']);
			$this->bind('quantity', $post['quantity']);
			$this->bind('buyerName', $post['buyerName']);
			$this->bind('userID', Other::axCodeReverse($post['userID']));
			$this->bind('phone', $post['phone']);
			$this->bind('passport', $post['passport']);
			$this->bind('shop', $post['shop']);
			$this->bind('price', $post['price']);
			$this->bind('weight', $post['weight']);
			$this->bind('flightID', $post['flightID']);
			$this->bind('id', $post['id']);

			$this->execute();
			header('location:'.ROOT_URL.'sort-packages/'.$post['id']);
		}

		if(isset($get['subroute']) AND !empty($get['subroute']) AND !isset($post['submitBarcode'])){
			$this->query("SELECT * FROM products WHERE id = ".$_GET['subroute']);
			$rows = $this->resultSet();
			return $rows;
		}

		if(isset($post['delete'])){
			foreach($_POST['productID'] as $item){
				$this->query("DELETE FROM products WHERE id = '$item'");
				$this->execute();
			}
		}

		if(isset($_POST['submitPayable'])){
			if($_POST['payablePassword'] == dondurma){
				$this->query("UPDATE products SET state = 'paid' WHERE id = ".$_POST['id']);
				$this->execute();
				Messages::setMsg('გადახდილია', 'success');
			}else{
				Messages::setMsg('პაროლი არასწორია', 'error');
			}
		}

		if(isset($post['obtained'])){
			if(!empty($_POST['productID'])){
				foreach($_POST['productID'] as $item){
					$this->query('SELECT id,state FROM products WHERE id = :id');
					$this->bind(':id', $item);
					$row = $this->single();
					// echo '<pre>';
					// print_r($row);
					// echo '</pre>';
					if(in_array('', $row)){
						$error = 1;
						Messages::setMsg('ამ მომხმარებელს გადასახდელი აქვს ამანათ(ებ)ის თანხა.', 'error');
						$this->query("SELECT * FROM products WHERE status = 'arrived'");
						$rows = $this->resultSet();
						return $rows;
					}
				}
			}
			if(!isset($error)){
				if(!empty($_POST['productID'])){
					foreach($_POST['productID'] as $item){
						$this->query("UPDATE products SET status = 'obtained' WHERE id = '$item'");
						$this->execute();
						$this->query("SELECT * FROM products WHERE id = '$item'");
						$rows[] = $this->resultSet();
					}

					foreach($rows as $it){
						$za[] = $it[0];
					}

					foreach($za as $key => $itm){
						$arr[$itm['userID']][$itm['productOrder']] = $itm;
					}

					foreach($arr as $userID => $trackingCode){
						echo $userID.'<br />';
						$la = '';
						foreach($trackingCode as $k => $a){
							$la[] = $k;
						}

						// $message = 'ამანათ(ებ)ი თრექინგ კოდ(ებ)ით '.implode(', ',$la).' გატანილია. გთხოვთ დაადეკლარიროთ.';
						// $this->statusMailer($userID, '999', $message);

						header('location:'.ROOT_URL.'packages/obtained/');
					}
				}
			}
		}
	}

	public function countPackagesInFlightSorting($axcode, $flightID){
		$this->query("SELECT * FROM products WHERE flightID = :flightID AND userID = :userID AND sorting = '0'");
		$this->bind('flightID', $flightID);
		$this->bind('userID', $axcode);
		$rows = $this->resultSet();
		return $rows;
	}

	public function countPackagesInFlight($axcode, $flightID){
		$this->query('SELECT * FROM products WHERE flightID = :flightID AND userID = :userID');
		$this->bind('flightID', $flightID);
		$this->bind('userID', $axcode);
		$rows = $this->resultSet();
		return $rows;
	}

	public function invoice(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$exp = explode(' ', $get['id']);
		$this->query("SELECT * FROM products WHERE userID = $exp[0] AND  flightID = $exp[1]");
		$rows = $this->resultSet();
		return $rows;
	}

	public function selectProducts($prodyctType){
		if(empty($prodyctType)){
			$prodyctType = "WHERE status = 'obtained' ORDER BY id DESC";
		}
		$this->query('SELECT id FROM products '.$prodyctType);
		$rows = $this->resultSet();
		return $rows;
	}

	public function declarationOptions(){
		$this->query("SELECT * FROM declarOpts");
		$rows = $this->resultSet();
		return $rows;
	}

	//ელ ფოსტის და სმსების ტექსტები ბაზიდან
	public function dynamicTexts($status){
		$this->query("SELECT * FROM mails WHERE status = :status");
		$this->bind('status', $status);
		$row = $this->single();
		return $row;
	}

	//ელ ფოსტების გაგზავნა
	public function statusMailer($user = "", $message = ""){
		$this->query("SELECT * FROM users WHERE id = $user");
		$row = $this->single();

		$this->sms($row['phone'], $message);

		$to = $row['email'];

		$subject = 'ALIGEORGIA';

		$headers = "From: ALIGEORGIA\r\n";
		$headers .= "Reply-To: ALIGEORGIA\r\n";
		$headers .= "CC: aligeorgia@gmail.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		mail($to, $subject, $message, $headers);
	}

	//სმსების გაგზავნა
	public function sms($phone = "", $message = ""){
		$data = 'key='.urlencode('EB6D7303-51AC-4B18-9A0F-8CA31A62690E').'&destination='.urlencode($phone).'&sender=AliGeorgia&content='.urlencode($message);
		$url= "http://smsoffice.ge/api/send.aspx?".$data;
		$response = file_get_contents($url);
		return $response;
	}

	public function userDetails($userID){
		$this->query("SELECT * FROM users WHERE id = $userID");
		$row = $this->single();
		return $row;
	}

	public function edit(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		if(!isset($get['packageid'])){
			return false;
		}
		if(empty($get['packageid'])){
			return false;
		}
		$this->query('SELECT * FROM products WHERE id = :id');
		$this->bind('id', $get['packageid']);
		$row = $this->single();
		return $row;
	}

	public function update(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		if(!isset($get['packageid'])){return false;}
		if(empty($get['packageid'])){return false;}
		if(!isset($post)){return false;}
		if(empty($post)){return false;}
		$this->query('UPDATE products SET productName = :productName, quantity = :quantity, buyerNameEng = :buyerNameEng, userID = :userID, phone = :phone, passport = :passport, shop = :shop, price = :price, currency = :currency, weight = :weight, flightID = :flightID, status = :status WHERE id = :id');
		$this->bind('productName', $post['productName']);
		$this->bind('quantity', $post['quantity']);
		$this->bind('buyerNameEng', $post['buyerNameEng']);
		$this->bind('userID', Other::axCodeReverse($post['userID']));
		$this->bind('shop', $post['shop']);
		$this->bind('passport', $post['passport']);
		$this->bind('phone', $post['phone']);
		$this->bind('price', $post['price']);
		$this->bind('currency', $post['currency']);
		$this->bind('weight', $post['weight']);
		$this->bind('flightID', $post['flightID']);
		$this->bind('status', $post['status']);
		$this->bind('id', $get['packageid']);

		$this->execute();

		if ($this->rowCount()) {
			return Messages::setMsg('UPDATE_PACKAGE_SUCCESS', 'success');
		} else {
			return Messages::setMsg('UPDATE_PACKAGE_ERROR', 'error');
		}
	}
}
