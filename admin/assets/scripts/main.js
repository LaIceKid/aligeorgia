let checkAll = (element) => {
  if($(element).is(':checked')){
    $('.product-id-checkbox').prop('checked', true);
  }else{
    $('.product-id-checkbox').prop('checked', false);
  }
};

let checkAllUnderFlight = (element) => {
  console.log(element);
  $('.product-id-checkbox').prop("checked", false);
  $('.' + element.id).prop("checked", true);
};

let getData = (element, start, amount, delimiter) => {
  let paneldata = $('#'+element);
  let rooturl = paneldata.data('rooturl');
  let role = paneldata.data('role');
  let packagedata = 'packagedata';
  if(role === 'sent' || role === 'giveaway'){
    packagedata = 'packagedataflight';
  }
  let axcode = '';
  if($('#giveaway-axcode').length === 1){
    axcode = $('#giveaway-axcode').val();
  }
  $.ajax({
    type: 'GET',
    url: ''+rooturl+packagedata+'/'+delimiter+'/?status='+role+'&start='+start+'&amount='+amount+'&axcode='+axcode+'',
    data: {},
    cache: false,
    beforeSend(){
      paneldata.html('<div class="preloader-cog"><i class="fa fa-cog fa-spin fa-4x text-danger"></i></div>');
      $('.main-content').attr("style","height: calc(100vh - 55px)");
    },
    success: function(data){
      paneldata.html(data);
      $('.main-content').removeAttr("style");
      $('[data-toggle="tooltip"]').tooltip();
    }
  });
};

getData("data-panel", 0, 100);

let packageListClone = (element) => {
  var row = [];
  $('table tr').each(function () {
    var col = [];
    $(this).children("td").each(function () {
      col.push($(this).text());
    });
    row.push(col.join("\t"));
  });
  var data = row.join("\n");
  //window.clipboardData.setData('Text', data);
  //alert(data);
  $('#dataTextarea').val(data);
  $("#copy").trigger('click');
};
let invoiceListClone = (element) => {
  var row = [];
  $('table tr').each(function () {
    var col = [];
    $(this).children("td").each(function () {
      col.push($(this).text());
    });
    row.push(col.join("\t"));
  });
  var data = row.join("\n");
  //window.clipboardData.setData('Text', data);
  //alert(data);
  $('#dataTextarea').val(data);
  $("#copy").trigger('click');
};

let message_fade_out = () => {
  if ($(".message-display").length > 0){
    setTimeout(function(){
      $(".message-display").fadeOut(300);//FADE OUT MESSAGE

      setTimeout(function(){
        $(".message-display").remove();//REMOVE MESSAGE
      }, 500);

    }, 3000);
  }
};
message_fade_out();

let submit_package_form = (formToSubmit) => {
  $('.'+formToSubmit).submit();
};

let sortPackage = () => {
  if($(".command-disable-inputs").length === 1){
    $("#code").focus();
    $("td input[type=text]").css({border:0}).prop('disabled', true);
    $("#sorting-save").prop('disabled', true);
  }
  if($(".focus-weight").length === 1){
    $("input[name=weight]").focus();
  }
};
sortPackage();

let copyUsers = (element) => {
  $("#copy-users-preloader").fadeIn(function(){
    cloneUsers(element);
  });
};

let cloneUsers = (element) => {
  let row = [];
  $('table tr').each(function () {
    let col = [];
    $(this).children("td").each(function () {
      col.push($(this).text());
    });
    row.push(col.join("\t"));
  });
  let data = row.join("\n");
  $('#'+$(element).data("element")).val(data);
  $("#copy-users-preloader").fadeOut(500);
};

let editUser = (element) => {
  let rooturl = $(element).data("rooturl");
  let userid = $(element).data("userid");
  $.ajax({
    type: 'GET',
    url: ''+rooturl+'edit-user/?userid='+userid,
    data: {},
    cache: false,
    beforeSend(){
      if($("#edit-user-"+userid+"-data").length === 0){
        $("#edit-user-"+userid).after('<tr class="edit-user-data" id="edit-user-'+userid+'-data"><td class="preloader-cog" colspan="100"><i class="fa fa-cog fa-spin fa-3x text-danger"></i></td></tr>');
      }
    },
    success: function(data){
      $("#edit-user-"+userid+"-data").hide().html(data).fadeIn();
    }
  });
};

let updateUser = (element) => {
  let rooturl = $(element).data("rooturl");
  let userid = $(element).data("userid");
  let inputs = $("#edit-user-"+userid+"-data :input");
  $("#tbl2 :input").serialize();
  $.ajax({
    type: 'POST',
    url: ''+rooturl+'update-user/?userid='+userid,
    data: inputs.serialize(),
    cache: false,
    beforeSend(){

    },
    success: function(data){
      $('#edit-user-'+userid+'-data').remove();
      $.each(inputs, function(key, index){
        $('#'+$(index).attr("name")+'-'+userid).html($(index).val());
      });

      $('.message-place').load(document.URL +  ' .message-display', function(){
        $(".message-display").hide().fadeIn();
        message_fade_out();
      });
    }
  });
};

let editPackage = (element) => {
  console.log(element);
  let rooturl = $(element).data("rooturl");
  let packageid = $(element).data("packageid");
  $.ajax({
    type: 'GET',
    url: ''+rooturl+'edit-package/?packageid='+packageid,
    data: {},
    cache: false,
    beforeSend(){
      if($("#edit-package-"+packageid+"-data").length === 0){
        $("#edit-package-"+packageid).after('<tr class="edit-package-data" id="edit-package-'+packageid+'-data"><td class="preloader-cog" colspan="100"><i class="fa fa-cog fa-spin fa-3x text-danger"></i></td></tr>');
      }
    },
    success: function(data){
      $("#edit-package-"+packageid+"-data").hide().html(data).fadeIn();
    }
  });
};

let updatePackage = (element) => {
  let rooturl = $(element).data("rooturl");
  let packageid = $(element).data("packageid");
  let inputs = $("#edit-package-"+packageid+"-data :input");
  $("#tbl2 :input").serialize();
  $.ajax({
    type: 'POST',
    url: ''+rooturl+'update-package/?packageid='+packageid,
    data: inputs.serialize(),
    cache: false,
    beforeSend(){

    },
    success: function(data){
      $('#edit-package-'+packageid+'-data').remove();
      $.each(inputs, function(key, index){
      let idStructure = $('#'+$(index).attr("name")+'-'+packageid);
        idStructure.html($(index).val());
        console.log(idStructure.html($(index).val()));
      });

      $('.message-place').load(document.URL +  ' .message-display', function(){
        $(".message-display").hide().fadeIn();
        message_fade_out();
      });
    }
  });
};