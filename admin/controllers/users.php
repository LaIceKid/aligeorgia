<?php

class Users extends controller{
	protected function all(){
		$viewmodel = new UsersModel();
		$this->ReturnView($viewmodel->all(), true);
	}
	protected function copy(){
		$viewmodel = new UsersModel();
		$this->returnView($viewmodel->copy(), true);
	}
	protected function edit(){
		$viewmodel = new UsersModel();
		$this->returnView($viewmodel->edit(), false);
	}
	protected function update(){
		$viewmodel = new UsersModel();
		$this->returnView($viewmodel->update(), false);
	}
}