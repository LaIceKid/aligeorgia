<?php
class Package extends Controller{
	protected function awaiting(){
		$viewmodel = new PackageModel();
		$this->ReturnView($viewmodel->awaiting(), true);
	}
	protected function packagedata(){
		$viewmodel = new PackageModel();
		$this->ReturnView($viewmodel->packagedata(), false);
	}
	protected function packagedataflight(){
		$viewmodel = new PackageModel();
		$this->ReturnView($viewmodel->packagedataflight(), false);
	}
	protected function warehouse(){
		$viewmodel = new PackageModel();
		$this->ReturnView($viewmodel->warehouse(), true);
	}
	protected function pending(){
		$viewmodel = new PackageModel();
		$this->ReturnView($viewmodel->pending(), true);
	}
	protected function arrived(){
		$viewmodel = new PackageModel();
		$this->ReturnView($viewmodel->arrived(), true);
	}
	protected function obtained(){
		$viewmodel = new PackageModel();
		$this->ReturnView($viewmodel->obtained(), true);
	}
	protected function giveaway(){
		$viewmodel = new PackageModel();
		$this->ReturnView($viewmodel->giveaway(), true);
	}
	protected function invoice(){
		$viewmodel = new PackageModel();
		$this->ReturnView($viewmodel->giveaway(), true);
	}
	protected function transferPackages(){
		$viewmodel = new PackageModel();
		$viewmodel->transferPackages();
	}
	protected function sorting(){
		$viewmodel = new PackageModel();
		$this->ReturnView($viewmodel->sorting(), true);
	}
	protected function edit(){
		$viewmodel = new PackageModel();
		$this->ReturnView($viewmodel->edit(), false);
	}
	protected function update(){
		$viewmodel = new PackageModel();
		$this->ReturnView($viewmodel->update(), false);
	}
}
