<?php
class Flight extends Controller{
	protected function Index(){
		$viewmodel = new FlightModel();
		$this->ReturnView($viewmodel->Index(), true);
	}

	protected function invoice(){
		$viewmodel = new FlightModel();
		$this->ReturnView($viewmodel->invoice(), true);
	}
	
	protected function all(){
		$viewmodel = new FlightModel();
		$this->ReturnView($viewmodel->all(), true);
	}

	protected function display(){
		$viewmodel = new FlightModel();
		$this->ReturnView($viewmodel->display(), true);
	}

	protected function packageList(){
		$viewmodel = new FlightModel();
		$this->ReturnView($viewmodel->packageList(), true);
	}

	protected function edit(){
		$viewmodel = new FlightModel();
		$this->ReturnView($viewmodel->edit(), true);
	}

	protected function delete(){
		$viewmodel = new FlightModel();
		$viewmodel->delete();
	}

	protected function invoiceList(){
		$viewmodel = new FlightModel();
		$this->ReturnView($viewmodel->invoiceList(), true);
	}
}
?>
