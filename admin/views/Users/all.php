<div class="panel">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="fa fa-user"></span>
            მომხმარებლები
        </h4>
    </div>
    <div class="panel-body row">
        <div class="col-lg-3">
            <a class="btn btn-success form-control" style="color:white;"
               href="<?php echo ROOT_URL; ?>copy-users/">მომხმარებლების
                კლონირება</a>
        </div>

        <div class="col-lg-3">
            <a class="btn btn-default form-control"
               href="<?php echo ROOT_URL; ?>all-users/physical/">ფიზიკური
                პირი</a>
        </div>
        <div class="col-lg-3">
            <a class="btn btn-default form-control"
               href="<?php echo ROOT_URL; ?>all-users/legal/">იურიდიული
                პირი</a>
        </div>
        <div class="col-lg-3">
            <a class="btn btn-default form-control"
               href="<?php echo ROOT_URL; ?>all-users/entrepreneur/">იურიდიული
                პირი
                (ინდმეწარმე)</a>
        </div>
    </div>
</div>

<div class="message-place"></div>

<div class="panel">
    <div class="panel-heading">
        <h4 class="panel-title">
            მომხმარებლების
            სია</h4>
    </div>
    <div class="panel-body table-responsive custom-panel-body"
         id="giveaway-data-panel"
         data-rooturl="<?php echo ROOT_URL; ?>">
        <table class="table table-condensed prod-table">
            <thead>
            <tr>
                <th>ID</th>
                <th>სახელი</th>
                <th>სახელი(Eng)</th>
                <th>ელ.ფოსტა</th>
                <th>რეგ.თარიღი</th>
                <th>პ/ნ</th>
                <th>მისამართი</th>
                <th>დაბ.თარიღი</th>
                <th>ტელეფონი</th>
                <th>რედაქ.</th>
                <th>წაშლა</th>
                <th>ბლოკი</th>
            </tr>
            </thead>
            <tbody>
			<?php foreach ($viewmodel as $key => $item) { ?>
                <tr id="edit-user-<?php echo $item['id']; ?>" class="edit-users">
                    <td><?php $other = new other();
						echo Other::AxCodeUser($item['id']); ?></td>
                    <td id="name-<?php echo $item['id']; ?>"><?php echo $item['name']; ?></td>
                    <td id="nameEn-<?php echo $item['id']; ?>"><?php echo $item['nameEn']; ?></td>
                    <td id="email-<?php echo $item['id']; ?>"><?php echo $item['email']; ?></td>
                    <td id="register_date-<?php echo $item['id']; ?>"><?php echo $item['register_date']; ?></td>
                    <td id="passport-<?php echo $item['id']; ?>"><?php echo $item['passport']; ?></td>
                    <td class="ellipsis" id="address-<?php echo $item['id']; ?>">
                        <div data-toggle="tooltip" data-placement="top" title="<?php echo $item['address']; ?>">
                            <?php echo $item['address']; ?>
                        </div>
                    </td>
                    <td id="birthday-<?php echo $item['id']; ?>"><?php echo $item['birthday']; ?></td>
                    <td id="phone-<?php echo $item['id']; ?>"><?php echo $item['phone']; ?></td>
                    <td class="text-center">
                        <button class="btn btn-xs btn-warning"
                                onclick="editUser(this)"
                                data-userid="<?php echo $item['id']; ?>"
                                data-rooturl="<?php echo ROOT_URL; ?>">
                            <span class="ti-pencil"></span>
                        </button>
                    </td>
                    <td class="text-center">
                        <a href="<?php echo ROOT_PATH; ?>delete-user/<?php echo $item['id'] ?>"
                           class="btn btn-xs btn-danger"
                           onclick="return confirm('ნამდვილად გსურთ ამ მომხმარებლის წაშლა ?');"><span
                                    class="ti-trash"></span></a>
                    </td>
                    <td class="text-center">
                        <a href="<?php echo ROOT_PATH; ?>ban-user/<?php echo $item['id'] ?>"
                           class="btn btn-xs btn-danger"
                           onclick="return confirm('ნამდვილად გსურთ ამ მომხმარებლის დაბლოკვა ?');"><span
                                    class="ti-na"></span></a>
                    </td>
                </tr>
			<?php } ?>
            </tbody>
        </table>
    </div>
</div>