<div class="panel">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="fa fa-user"></span>
            მომხმარებლები
        </h4>
    </div>
    <div class="panel-body row" style="padding-bottom:7px;">
        <button class="btn btn-success"
                onclick="copyUsers(this)"
                data-element="data-textarea">
            <span class="ti-files"></span>
            კოპირება
            <i class="fa fa-cog fa-spin fa-1x fa-fw margin-bottom" id="copy-users-preloader"></i>
        </button>
        <a href="<?php echo ROOT_URL; ?>all-users/" class="btn btn-default">
            <span class="ti-angle-double-left"></span>
            "მომხმარებლები"-ში და ბრუნება
            <i class="fa fa-cog fa-spin fa-1x fa-fw margin-bottom" id="copy-users-preloader"></i>
        </a>
        <br><br>
        <label for="dataTextarea">კლონირებული
            ინფორმაცია</label>
        <textarea
                id="data-textarea"
                class="form-control"
                rows="20"></textarea>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">
        <h4 class="panel-title">
            მომხმარებლების
            სია</h4>
    </div>
    <div class="panel-body table-responsive custom-panel-body"
         id="giveaway-data-panel"
         data-rooturl="<?php echo ROOT_URL; ?>">
        <table class="dataTextareaTable table table-striped">
            <thead>
            <tr>
                <th>AX</th>
                <th>სახელი</th>
                <th>სახელი Eng</th>
                <th>ელ.ფოსტა</th>
                <th>მისამართი</th>
                <th>ტელეფონის ნომერი</th>
                <th>ბანი</th>
            </tr>
            </thead>
            <tbody>
			<?php foreach ($viewmodel as $users): ?>
                <tr>
                    <td><?php echo Other::AxCodeUser($users['id']); ?></td>
                    <td><?php echo $users['name']; ?></td>
                    <td><?php echo $users['nameEn']; ?></td>
                    <td><?php echo $users['email']; ?></td>
                    <td><?php echo $users['address']; ?></td>
                    <td><?php echo $users['phone']; ?></td>
                    <td><?php echo $users['ban']; ?></td>
                </tr>
			<?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>