<form action="<?php echo ROOT_URL; ?>transfer-packages/?status=arrived&success-header=arrived&error-header=pending" method="post" class="panding-package-form">
    <div class="panel">
      <div class="panel-heading">
        <h4 class="panel-title">ამანათ(ებ)ის მართვა</h4>
      </div>
      <div class="panel-body">

        <div class="col-lg-3">
            <p>სტატუსი "ჩამოსული"</p>

            დოლარის კურსი <input type="text" class="form-control" value="2.93">
            თარიღი <input type="text" class="form-control" value="<?php echo date('Y-m-d'); ?>">
            <br>
            <button type="button" name="button" class="btn btn-success col-md-12" onclick="submit_package_form('panding-package-form')">მონიშნულის გადატანა</button>
        </div>

        <div class="col-lg-2">
            <p>E-mail და SMS გაგზავნა</p>
            <label class="switch-input">
                <input type="checkbox" name="sms-email" checked="">
                <i data-swon-text="კი" data-swoff-text="არა" style="font-family:'BPG_GEL_Excelsior_Caps'"></i> გავგზავნოთ?
            </label>
        </div>
      </div>
    </div>

    <div class="panel">
      <div class="panel-heading">
        <h4 class="panel-title">გამოგზავნილი ამანათები</h4>
      </div>
      <div class="panel-body table-responsive custom-panel-body" id="data-panel" data-role="sent" data-rooturl="<?php echo ROOT_URL; ?>">

      </div>
    </div>
</form>