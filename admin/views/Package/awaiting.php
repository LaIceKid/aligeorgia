<div class="panel">
  <div class="panel-heading">
    <h4 class="panel-title">პროდუქციის დამატება</h4>
  </div>
  <div class="panel-body">
    <div class="form-group col-lg-2">
      <input type="text" class="form-control" placeholder="თრექინგ კოდი">
    </div>
    <div class="form-group col-lg-2">
      <select class="form-control">
        <option value="" disabled selected>საქონლის დასახელება</option>
        <option value="სხვადასხვა ელექტრონული მოწყობილებები">სხვადასხვა ელექტრონული მოწყობილებები</option><option value="ჩანთები და ჩასადებები">ჩანთები და ჩასადებები</option><option value="ფეხსაცმელი">ფეხსაცმელი</option><option value="ტელეფონი და ქსელური მოწყობილობები">ტელეფონი და ქსელური მოწყობილობები</option><option value="ტანსაცმელი, ყველა ტიპის სამოსი">ტანსაცმელი, ყველა ტიპის სამოსი</option><option value="საკვები დანამატები">საკვები დანამატები</option><option value="სათამაშოები და სპორტული ინვენტარი">სათამაშოები და სპორტული ინვენტარი</option><option value="საათები">საათები</option><option value="პარფიუმერია და კოსმეტიკა">პარფიუმერია და კოსმეტიკა</option><option value="ოპტიკური და ფოტო აპარატურა">ოპტიკური და ფოტო აპარატურა</option><option value="ნაბეჭდი პროდუქცია, წიგნები, ბროშურები">ნაბეჭდი პროდუქცია, წიგნები, ბროშურები</option><option value="მუსიკალური ინსტრუმენტები და მათი ნაწილები">მუსიკალური ინსტრუმენტები და მათი ნაწილები</option><option value="მინის ნაწარმი">მინის ნაწარმი</option><option value="მედიკამენტები">მედიკამენტები</option><option value="კომპიუტერი, ლეპტოპი და მათი ნაწილები">კომპიუტერი, ლეპტოპი და მათი ნაწილები</option><option value="იარაღები და ხელის ინსტრუმენტები">იარაღები და ხელის ინსტრუმენტები</option><option value="განათება, ჭაღები, ლამპები, ფარები">განათება, ჭაღები, ლამპები, ფარები</option><option value="ბიჟუტერია">ბიჟუტერია</option><option value="ავტო ნაწილები">ავტო ნაწილები</option><option value="სხვადასხვა მზა ნაწარმი">სხვადასხვა მზა ნაწარმი</option>
      </select>
    </div>
    <div class="form-group col-lg-1">
      <input type="text" class="form-control" placeholder="რაოდ.">
    </div>
    <div class="form-group col-lg-1">
      <input type="text" class="form-control" placeholder="მაღაზია">
    </div>
    <div class="form-group col-lg-1">
      <input type="text" class="form-control" placeholder="ფასი">
    </div>
    <div class="form-group col-lg-2">
      <input type="text" class="form-control" placeholder="მომხმარებელი">
    </div>
    <div class="form-group col-lg-1">
      <select class="form-control">
        <option value="" disabled selected>¥₾$</option>
        <option value="cny">CNY</option>
        <option value="gel">GEL</option>
        <option value="usd">USD</option>
      </select>
    </div>
    <div class="form-group col-lg-1">
      <button type="submit" name="submit" class="btn btn-success">დამატება</button>
    </div>
  </div>
</div>

<form action="<?php echo ROOT_URL; ?>transfer-packages/?status=china-stock&success-header=warehouse&error-header=awaiting" method="post" class="awaiting-package-form">
    <div class="panel">
      <div class="panel-heading">
        <h4 class="panel-title">ამანათ(ებ)ის მართვა</h4>
      </div>
      <div class="panel-body">

        <div class="col-lg-3">
                <p>სტატუსი "მისულია ჩინეთის საწყობში"</p>
                <label>
                    <button type="button" name="button" class="btn btn-success col-md-12" onclick="submit_package_form('awaiting-package-form')">მონიშნულის გადატანა</button>
                </label>
            </div>

        <div class="col-lg-2">
                <p>E-mail და SMS გაგზავნა</p>
                <label class="switch-input">
                    <input type="checkbox" name="sms-email" checked="">
                    <i data-swon-text="კი" data-swoff-text="არა" style="font-family:'BPG_GEL_Excelsior_Caps'"></i> გავგზავნოთ?
                </label>
            </div>

        <div class="col-lg-6">
          <p>დამატებულია:</p>
          <label>
            <div class="btn btn-primary" data-subroute="frontend" onclick="getData('data-panel', 0, 100, 'frontend')">მომხმარებლის <label class="label label-default label-transparent" style="background:#fff;"><?php echo $PackageModel->BackFrontCounter(''); ?></label></div>
            <div class="btn btn-primary" data-subroute="list" onclick="getData('data-panel', 0, 100, 'list')">ადმინისტრატორის <label class="label label-default label-transparent" style="background:#fff;"><?php echo $PackageModel->BackFrontCounter('list'); ?></label></div>
          </label>
        </div>

      </div>
    </div>

    <div class="panel">
      <div class="panel-heading">
        <h4 class="panel-title">ამანათები მოლოდინის რეჟიმში</h4>
      </div>
          <div class="panel-body table-responsive custom-panel-body" id="data-panel" data-role="awaiting" data-rooturl="<?php echo ROOT_URL; ?>">

          </div>
    </div>
</form>
