<form action="<?php echo ROOT_URL; ?>sort-packages/" method="post" class="arrived-package-form">
    <div class="panel">
        <div class="panel-heading">
            <h4 class="panel-title">
                <span class="fa fa-barcode"></span>
                ამანათების
                დახარისხება
            </h4> </div>
        <div class="panel-body barcode-scanner row">
            <label class="col-md-8">
                <input type="text" class="form-control input-lg" placeholder="შეიყვანეთ თრექინგ კოდი" name="code" id="code" autofocus> </label>
            <input type="submit" class="btn btn-success input-lg col-md-3" name="submitBarcode" value="Barcode It!" /> </div>
    </div>
</form>
<?php $Packages = new PackageModel(); ?>
<form action="" method="post">
    <div class="panel">
        <div class="panel-body table-responsive custom-panel-body">
            <table class="table table-hover table-condensed table-fullwidth text-nowrap">
                <thead>
                <?php //print_r($_GET); ?>
                <?php //print_r($_POST); ?>
                    <?php if (!empty($viewmodel) && count($viewmodel)>= 1) {
                        $sorting = $Packages->countPackagesInFlightSorting($viewmodel[0]['userID'], $viewmodel[0]['flightID']);
                        $countPackagesInThisFlight = $countPackages = $Packages->countPackagesInFlight($viewmodel[0]['userID'], $viewmodel[0]['flightID']);
                    ?>
                    <tr class="green">
                        <th colspan="100" style="font-size:26px;">
                            <?php if (count($sorting) <= 0){echo 'სამსეა ';}else{ ?>
                                ამ მომხმარებელს სულ აქვს ამ რეისში <span style="color:red; font-size:36px;"><?php echo count($countPackagesInThisFlight); ?></span> ამანათი
                            <?php } ?>
                        </th>
                    </tr>
                    <tr>
                        <th>
                            <div class="fancy-checkbox custom-bgcolor-green">
                                <label>
                                    <input type="checkbox" onchange="checkAll(this)" name="chk[]" /> <span></span>
                                </label>
                            </div>
                        </th>
                        <th> თრექინგი </th>
                        <th> დასახელება </th>
                        <th> რაოდ. </th>
                        <th> მყიდველი </th>
                        <th> AX კოდი </th>
                        <th> ტელეფონი </th>
                        <th> პასსპორტი </th>
                        <th> მაღაზია </th>
                        <th> თანხა </th>
                        <th> წონა </th>
                        <th> რეისი </th>
                        <th> Status </th>
                        <th> File </th>
                        <th class="actions" colspan="3"> Actions </th>
                    </tr>

                </thead>
                <tbody>
                    <?php foreach ($viewmodel as $item) { ?>
                        <?php foreach ($viewmodel as $item) { ?>
                            <tr>
                                <td>
                                    <div class="fancy-checkbox custom-bgcolor-green">
                                        <label>
                                            <input type="checkbox" name="productID[]" value="<?php echo $item['id']; ?>" class="product-id-checkbox" /> <span></span>
                                            <input type="hidden" name="id" value="<?php echo $item['id']; ?>" />
                                        </label>
                                    </div>
                                </td>
                                <td class="office-<?php echo $item['office']; ?>">
                                    <input type="text" name="productOrder" value="<?php echo $item['productOrder']; ?>" class="form-control input-sm inputz" />
                                </td>
                                <td>
                                    <input type="text" name="productName" value="<?php echo $item['productName']; ?>" class="form-control input-sm" />
                                </td>
                                <td>
                                    <input type="text" name="quantity" value="<?php echo $item['quantity']; ?>" class="form-control input-sm" /> </td>
                                <td>
                                    <input type="text" name="buyerName" value="<?php echo $item['buyerName']; ?>" class="form-control input-sm inputz2" />
                                </td>
                                <td>
                                    <input type="text" name="userID" style="width:80px;" value="<?php $other = new other();
                                    echo $other->AxCodeUser($item['userID']); ?>" class="form-control input-sm" />
                                </td>
                                <td>
                                    <input type="text" name="phone" value="<?php echo $item['phone']; ?>" class="form-control input-sm" />
                                </td>
                                <td>
                                    <input type="text" name="passport" value="<?php echo $item['passport']; ?>" class="form-control input-sm" />
                                </td>
                                <td>
                                    <input type="text" name="shop" value="<?php echo $item['shop']; ?>" class="form-control input-sm" />
                                </td>
                                <td>
                                    <input type="text" name="price" value="<?php echo $item['price']; ?>" class="form-control input-sm" />
                                </td>
                                <td>
                                    <input type="text" style="width:70px" name="weight" autofocus class="form-control input-sm" placeholder="<?php echo $item['weight']; ?>" />
                                </td>
                                <td>
                                    <input type="text" style="width:60px" name="flightID" value="<?php echo $item['flightID']; ?>" class="form-control input-sm" />
                                </td>
                                <td>
                                    <?php echo $item['status']; ?>
                                </td>
                                <td>
                                    <?php if ($item[ 'file']=='-' || empty($item[ 'file'])) { ?>
                                        ---
                                    <?php } else { ?>
                                        <a class="btn btn-default btn-xs viewFile" href="<?php echo ROOT_PATH; ?>../upload/<?php echo $item['file']; ?>" target="_blank"><span class="ti-image" aria-hidden="true"></span></a>
                                    <?php } ?>
                                </td>
                                <td>
                                    <a href="<?php echo ROOT_URL . 'packages/invoice/' . $item['userID'] . '+' . $item['flightID']; ?>" class="btn btn-xs btn-info">
                                        <span class="ti-file"></span>
                                    </a>
                                    <button type="submit" id="sorting-save" class="btn btn-xs btn-warning" name="save">
                                        <span class="ti-save"></span>
                                    </button>
                                    <a href="<?php echo ROOT_PATH; ?>home/delete/<?php echo $item['id'] ?>" class="btn btn-xs btn-danger" onclick="return confirm('ნამდვილად გსურთ ამ ამანათის წაშლა ?');">
                                        <span class="ti-trash"></span></a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</form>

<!--FOR JS-->
<?php if(isset($_GET['subroute']) && !empty($_GET['subroute'])){ ?>
    <div class="command-disable-inputs" id="<?php echo $_GET['subroute']; ?>"></div>
<?php } ?>
<?php if(isset($_POST['code']) && !empty($_POST['code'])){ ?>
    <div class="focus-weight" id="<?php echo $_POST['code']; ?>"></div>
<?php } ?>
<!--FOR JS-->