<div class="message-place"></div>
<table class="table table-hover table-condensed table-fullwidth text-nowrap">
    <thead>
    <tr>
        <th>
            <div class="fancy-checkbox custom-bgcolor-green">
                <label>
                    <input type="checkbox"
                           onchange="checkAll(this)"
                           name="chk[]"/>
                    <span></span>
                </label>
            </div>
        </th>
        <th>
            თრექინგ
            კოდი
        </th>
        <th>
            დასახელება
        </th>
        <th>
            რაოდ.
        </th>
        <th>
            მყიდველი
        </th>
        <th>
            AX
            კოდი
        </th>
        <th>
            ტელეფონი
        </th>
        <th>
            პასსპორტი
        </th>
        <th>
            მაღაზია
        </th>
        <th>
            თანხა
        </th>
        <th>
            ვალუტა
        </th>
        <th>
            წონა
        </th>
        <th>
            რეისი
        </th>
        <th>
            Status
        </th>
        <th>
            ფაილი
        </th>
        <th colspan="3"
            class="actions">
            Actions
        </th>
        <th></th>
    </tr>
    </thead>
    <tbody>
	<?php foreach ($viewmodel as $item) { ?>
        <tr id="edit-package-<?php echo $item['id']; ?>" class="edit-package">
            <td>
                <div class="fancy-checkbox custom-bgcolor-green">
                    <label>
                        <input type="checkbox"
                               name="productID[]"
                               value="<?php echo $item['id']; ?>"
                               class="product-id-checkbox"/>
                        <span></span>
                    </label>
                </div>
            </td>
            <td class="office-<?php echo !empty($item['office'])?$item['office']:'unknown'; ?>"><?php echo $item['productOrder']; ?></td>
            <td class="ellipsis" id="productName-<?php echo $item['id']; ?>">
                <div data-toggle="tooltip" data-placement="top" title="<?php echo $item['productName']; ?>">
			        <?php echo $item['productName']; ?>
                </div>
            </td>
            <td id="quantity-<?php echo $item['id']; ?>"><?php echo $item['quantity']; ?></td>
            <td id="buyerNameEng-<?php echo $item['id']; ?>"><?php echo $item['buyerNameEng']; ?></td>
            <td id="userID-<?php echo $item['id']; ?>"><?php echo Other::AxCodeUser($item['userID']); ?></td>
            <td id="phone-<?php echo $item['id']; ?>"><?php echo $item['phone']; ?></td>
            <td id="passport-<?php echo $item['id']; ?>"><?php echo $item['passport']; ?></td>
            <td class="ellipsis" id="shop-<?php echo $item['id']; ?>"><?php echo $item['shop']; ?></td>
            <td id="price-<?php echo $item['id']; ?>"><?php echo $item['price']; ?></td>
            <td id="currency-<?php echo $item['id']; ?>"><?php echo $item['currency']; ?></td>
            <td id="weight-<?php echo $item['id']; ?>"><?php echo $item['weight']; ?></td>
            <td>
                ---
            </td>
            <td id="status-<?php echo $item['id']; ?>"><?php echo $item['status']; ?></td>
            <td>
				<?php if ($item['file'] == '-' || empty($item['file'])) { ?>
                    ---
				<?php } else { ?>
                    <a class="btn btn-default btn-xs viewFile"
                       href="<?php echo ROOT_PATH; ?>../assets/images/upload/<?php echo $item['file']; ?>"
                       target="_blank"><span
                                class="ti-image"
                                aria-hidden="true"></span></a>
				<?php } ?>
            </td>
            <td>
                <a href="<?php echo ROOT_URL . 'packages/invoice/' . $item['userID'] . '+' . $item['flightID']; ?>"
                   class="btn btn-xs btn-info"><span
                            class="ti-file"></span></a>
            </td>
            <td>
                <span class="btn btn-xs btn-warning"
                        onclick="editPackage(this)"
                        data-packageid="<?php echo $item['id']; ?>"
                        data-rooturl="<?php echo ROOT_URL; ?>">
                    <span class="ti-pencil"></span>
                </span>
            </td>
            <td>
                <a href="<?php echo ROOT_PATH; ?>home/delete/<?php echo $item['id'] ?>"
                   class="btn btn-xs btn-danger"
                   onclick="return confirm('ნამდვილად გსურთ ამ ამანათის წაშლა ?');"><span
                            class="ti-trash"></span></a>
            </td>
            <td>
                <!-- this is just for placeholder -->
            </td>
        </tr>
	<?php } ?>
    </tbody>
</table>
