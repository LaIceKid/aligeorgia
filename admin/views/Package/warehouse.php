<form action="<?php echo ROOT_URL; ?>transfer-packages/?status=sent&success-header=pending&error-header=warehouse" method="post" class="warehouse-package-form">
    <div class="panel">
  <div class="panel-heading">
    <h4 class="panel-title">ამანათ(ებ)ის მართვა</h4>
  </div>
  <div class="panel-body">

    <div class="col-lg-3">
			<p>სტატუსი "გამოგზავნილი"</p>
			<label>
                <div class="input-group">
                    <input class="form-control" type="text" name="tariff" placeholder="ტარიფი" value="0" required>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button" onclick="submit_package_form('warehouse-package-form')">მონიშნულის გადატანა</button>
                    </span>
                </div>
			</label>
		</div>

    <div class="col-lg-2">
        <p>E-mail და SMS გაგზავნა</p>
        <label class="switch-input">
            <input type="checkbox" name="sms-email" checked="">
            <i data-swon-text="კი" data-swoff-text="არა" style="font-family:'BPG_GEL_Excelsior_Caps'"></i> გავგზავნოთ?
        </label>
    </div>
  </div>
</div>

<div class="panel">
  <div class="panel-heading">
    <h4 class="panel-title">ამანათები ჩინეთის საწყობში</h4>
  </div>
  <div class="panel-body table-responsive custom-panel-body" id="data-panel" data-role="china-stock" data-rooturl="<?php echo ROOT_URL; ?>">

  </div>
</div>
</form>