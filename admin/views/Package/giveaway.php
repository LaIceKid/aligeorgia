    <div class="panel">
      <div class="panel-heading">
        <h4 class="panel-title"><span class="fa fa-barcode"></span> BARCODE</h4>
      </div>
      <div class="panel-body barcode-scanner row">
          <label class="col-md-8">
              <input type="text" class="form-control input-lg" id="giveaway-axcode" value="" placeholder="შეიყვანეთ AX კოდი">
          </label>
          <button class="btn btn-success input-lg col-md-3" onclick="getData('data-panel', 0, 100, 'list')">
              GO!
          </button>
      </div>
    </div>

    <div class="panel">
      <div class="panel-heading">
        <h4 class="panel-title">ამანათების გაცემა</h4>
      </div>
        <div class="panel-body table-responsive custom-panel-body" id="data-panel" data-role="giveaway" data-rooturl="<?php echo ROOT_URL; ?>">

        </div>
    </div>
