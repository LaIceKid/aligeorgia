<div class="rightBlock">
    <table class="table table-striped">
		<?php
		foreach ($viewmodel as $item) {
			?>
            <tr>
                <td>
                    <h3>
                        რეისის
                        № <?php Other::AxCode($item['id']); ?></h3>
                    <br/>

					<?php if ($item['id'] == $_GET['subroute']): ?>

                        <form action=""
                              method="post"
                              class="editFlight">

                            <div class="form-group">
                                <label for="datepicker-start"
                                       class="datepicker-start">პირველი
                                    თარიღი</label>
                                <input type="text"
                                       id="datepicker-start-first"
                                       name="firstDate"
                                       value="<?php echo $item['startDate'] ?>"
                                       placeholder="სავარაუდო თარიღი"
                                       class="form-control"/>
                            </div>

                            <div class="form-group">
                                <label for="datepicker-start"
                                       class="datepicker-start">სავარაუდო
                                    თარიღი</label>
                                <input type="text"
                                       id="datepicker-start"
                                       name="startDate"
                                       value="<?php echo $item['date'] ?>"
                                       placeholder="სავარაუდო თარიღი"
                                       class="form-control"/>
                            </div>

                            <div class="form-group">
                                <label for="datepicker-start2"
                                       class="datepicker-start2">საბოლოო
                                    თარიღი</label>
                                <input type="text"
                                       id="datepicker-start2"
                                       name="endDate"
                                       value="<?php echo $item['endDate'] ?>"
                                       placeholder="ჩამოსვლის თარიღი"
                                       class="form-control"/>
                            </div>
                            <input type="submit"
                                   name="dates"
                                   class="btn btn-success"
                                   value="რედაქტირება"/>

                        </form>

					<?php endif; ?>
                </td>

            </tr>
			<?php
		}
		?>
    </table>
</div>