<?php $flightModel = new FlightModel(); ?>
<div class="rightBlock">
	<form action="" method="post" class="editFlight">
		<div class="form-group">
		  <label for="datepicker-start" class="datepicker-start">გაგზავნის თარიღი</label>
		 <input type="text" id="datepicker-start" name="startDate" placeholder="გაგზავნის თარიღი" class="form-control"/>
		</div>
			<input type="submit" name="add" class="btn btn-success" value="რედაქტირება" />
	</form><br/>

	<h3>რეისები</h3>
	<table class="table table-striped">
    <?php foreach($viewmodel as $item){ ?>
			<tr>
				<th class="max">
					<a href="<?php echo ROOT_PATH; ?>display-flight-packages/<?php echo $item['id']; ?>">რეისის № <?php Other::AxCode($item['id']); ?></a>
                    <i>ფრენა: <?php echo $item['date']; ?> | ჩამოსვლა: <?php echo $item['endDate']; ?> | წონა: <?php echo $flightModel->sumWeight($item['id']); ?> | ამანათების რაოდენობა: <?php echo $flightModel->sumPackages($item['id']); ?></i>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight-invoices/<?php echo $item['id']; ?>" class="btn btn-info btn-xs">
						ინვოისი
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight-package-list-clone/<?php echo $item['id']; ?>" class="btn btn-danger btn-xs">
						ამანათების სია
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight-invoice-list-clone/<?php echo $item['id']; ?>" class="btn btn-danger btn-xs">
						ინვოისების სია
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight-edit/<?php echo $item['id']; ?>" class="btn btn-danger btn-xs">
						რედაქტირება
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight/delete/<?php echo $item['id']; ?>" class="btn btn-danger btn-xs" onclick="return confirm('ნამდვილად გსურთ ამ რეისის წაშლა ?');">
						წაშლა
					</a>
				</th>
			</tr>
    <?php } ?>
	</table>
	<a href="<?php echo ROOT_URL.'flight/all/'; ?>" class="btn btn-info">ყველას ნახვა</a>
</div>
