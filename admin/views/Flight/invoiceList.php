<div class="rightBlock">

	<style>
		.test tr td{
			padding:5px;
		}
	</style>

	<button class="btn btn-default" id="clone" onclick="invoiceListClone(this)">კლონირება</button>
	<button class="btn btn-default" id="copy" style="display:none;">კოპირება</button><br /><br />
	<textarea id="dataTextarea" style="width:100%;min-height:300px;"></textarea>
	<table class="dataTextareaTable table table-striped">
    <table class="table table-condensed prod-table">
      <thead>
      <tr>
        <th>თარიღი</th>
        <th>ინვოისის ნომერი</th>
        <th>სახელი გვარი</th>
        <th>პირადი ნომერი</th>
        <th>თრექინგ კოდი</th>
        <th>წონა</th>
        <th>ტარიფი</th>
        <th>კურსი</th>
        <th>ტრანსპორტირების ღირებულებები</th>
      </tr>
      </thead>
      <tbody>
	<?php $totalWeight = 0; ?>
	<?php foreach($viewmodel as $item): ?>
		<tr>
      <td><?php echo $item['date'] ?></td>
			<td><?php echo $item['invID']; ?></td>
			<td><?php echo $item['name']; ?></td>
			<td><?php echo $item['passport']; ?></td>
			<td><?php echo $item['productOrder']; ?></td>
			<td><?php echo $item['weight']; ?></td>
			<td><?php echo $item['tariff']; ?></td>
			<td><?php echo $item['currentUSDrate']; ?></td>
			<td><?php echo $item['tariff'] * $item['weight']; ?></td>
		</tr>
		<?php $totalWeight += $item['weight']; ?>

	<?php endforeach; ?>
		<tr>
			<td colspan="9"><strong>წონის ჯამი: <?php echo $totalWeight; ?></strong></td>
		</tr>
    </tbody>
	</table>
</div>
