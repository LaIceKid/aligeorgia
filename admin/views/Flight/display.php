<?php $flightModel = new FlightModel(); ?>
<div class="rightBlock">
	<form action="" method="post" class="editFlight">
		<div class="form-group">
		  <label for="datepicker-start" class="datepicker-start">გაგზავნის თარიღი</label>
		 <input type="text" id="datepicker-start" name="startDate" placeholder="გაგზავნის თარიღი" class="form-control"/>
		</div>
			<input type="submit" name="add" class="btn btn-success" value="რედაქტირება" />
	</form><br/>

	<h3>რეისები</h3>
	<table class="table table-striped">
    <?php foreach($viewmodel as $item){ ?>
			<tr>
				<th class="max">
					<a href="<?php echo ROOT_PATH; ?>display-flight-packages/<?php echo $item['id']; ?>">რეისის № <?php Other::AxCode($item['id']); ?></a>
                    <i>ფრენა: <?php echo $item['date']; ?> | ჩამოსვლა: <?php echo $item['endDate']; ?> | წონა: <?php echo $flightModel->sumWeight($item['id']); ?> | ამანათების რაოდენობა: <?php echo $flightModel->sumPackages($item['id']); ?></i>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight-invoices/<?php echo $item['id']; ?>" class="btn btn-info btn-xs">
						ინვოისი
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight/packageList/<?php echo $item['id']; ?>" class="btn btn-danger btn-xs">
						ამანათების სია
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight/invoiceList/<?php echo $item['id']; ?>" class="btn btn-danger btn-xs">
						ინვოისების სია
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight/edit/<?php echo $item['id']; ?>" class="btn btn-danger btn-xs">
						რედაქტირება
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight/delete/<?php echo $item['id']; ?>" class="btn btn-danger btn-xs" onclick="return confirm('ნამდვილად გსურთ ამ რეისის წაშლა?');">
						წაშლა
					</a>
				</th>
			</tr>
	    <?php if($item['id'] == $_GET['subroute']){ ?>
            <tr>
                <td colspan="10">
                    <div>
                        <form action="<?php echo ROOT_PATH; ?>packages/sent/" method="post">
                            <div class="col-xs-2">
                                <input type="submit" name="arrived" class="btn btn-success" value="ჩამოსული" />
                            </div>
                            <div class="col-xs-2">
                                <select name="flightID" required class="form-control">
                                    <option value="">აირჩიეთ რეისი</option>
								    <?php foreach($flightModel->flCount() as $fl){ ?>
                                        <option value="<?php echo $fl['id']; ?>"><?php echo $fl['id']; ?></option>
								    <?php } ?>
                                </select>

                            </div>
                            <input type="submit" class="btn btn-success" value="გადატანა" name="redirect" />

                            <table class="table table-hover table-condensed table-fullwidth">
                                <thead>
                                <tr>
                                    <th>
                                        <div class="fancy-checkbox custom-bgcolor-green">
                                            <label>
                                                <input type="checkbox" onchange="checkAll(this)" name="chk[]" />
                                                <span></span>
                                            </label>
                                        </div>
                                    </th>
                                    <th>Order ID</th>
                                    <th>პროდუქცია</th>
                                    <th>მყიდველი</th>
                                    <th>თარიღი</th>
                                    <th>წონა</th>
                                    <th>ფასი</th>
                                    <th>სტატუსი</th>
                                    <th>ინვოისი</th>
                                </tr>
                                </thead>

							    <?php
							    $packagesArr = $flightModel->pullPackages($_GET['subroute']);
							    foreach($packagesArr as $product){

								    if($product['status'] == ''){
									    $class = 'class="label label-default"';
								    }
								    if($product['status'] == 'china-stock'){
									    $class = 'class="label label-info"';
								    }
								    if($product['status'] == 'sent'){
									    $class = 'class="label label-warning"';
								    }
								    if($product['status'] == 'arrived'){
									    $class = 'class="label label-primary"';
								    }
								    if($product['status'] == 'obtained'){
									    $class = 'class="label label-success"';
								    }
								    ?>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="fancy-checkbox custom-bgcolor-green">
                                                <label>
                                                    <input type="checkbox" name="productID[]" value="<?php echo $item['id'];?>" class="product-id-checkbox" />
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td><?php echo $product['productOrder']; ?></td>
                                        <td><?php echo $product['productName']; ?></td>
                                        <td><?php echo $product['buyerName']; ?></td>
                                        <td><?php echo $product['create_date']; ?></td>
                                        <td><?php echo $product['weight']; ?></td>
                                        <td><?php echo $product['price']; ?></td>
                                        <td>
                                            <span <?php echo $class;?> >
                                                <?php
                                                    if($product['status'] == ''){
                                                        echo 'მოლოდინის რეჟიმში';
                                                    }elseif($product['status'] == 'china-stock'){
                                                        echo 'მისულია ჩინეთის საწყობში';
                                                    }elseif($product['status'] == 'sent'){
                                                        echo 'გამოგზავნილი';
                                                    }elseif($product['status'] == 'obtained'){
                                                        echo 'გატანილი';
                                                    }elseif($product['status'] == 'arrived'){
                                                        echo 'ჩამოსული';
                                                    }
                                                ?>
                                            </span>
                                        </td>
                                        <td><a href="<?php echo ROOT_URL.'invoice/?userid='.$product['userID'].'&flightid='.$item['id']; ?>" class="btn btn-info btn-xs">ინვოისის ბეჭდვა</a></td>
                                    </tr>
                                    </tbody>
							    <?php } ?>
                            </table>
                        </form>
                    </div>
                </td>
            </tr>
	    <?php } ?>
    <?php } ?>
	</table>
	<a href="<?php echo ROOT_URL.'flight/all/'; ?>" class="btn btn-info">ყველას ნახვა</a>
</div>



