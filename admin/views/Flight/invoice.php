<div class="rightBlock">
<input name="b_print" type="button" class="ipt btn btn-info" onClick="printdiv('div_print');" value="ინვოისის ბეჭდვა">
<script language="javascript">
	function printdiv(printpage){
		var headstr = "<html><head><title></title></head><body>";
		var footstr = "</body></html>";
		var newstr = document.all.item(printpage).innerHTML;
		var oldstr = document.body.innerHTML;
		document.body.innerHTML = headstr+newstr+footstr;
		window.print();
		document.body.innerHTML = oldstr;
		return false;
	}
</script>

<div id="div_print">
	<?php $inv = new FlightModel(); ?>
	
<?php
	// echo '<pre>';
	// print_r($viewmodel);
	// echo '</pre>';
?>	
	<?php foreach($viewmodel as $uid): ?>
		<?php foreach($inv->invoiceOut($uid['userID'], $_GET['subroute']) as $last): ?>
			<!---------------------------------------------------------------------------------------------------------->
			<!---------------------------------------------------------------------------------------------------------->
			<!---------------------------------------------------------------------------------------------------------->
			<!---------------------------------------------------------------------------------------------------------->


<h3 style="text-align:center;">ანგარიშ-ფაქტურა (ინვოისი)</h3>
<br />
<br />
	<table class="table" style="width:100%">
		<tr>
			<td style="width:50%">
				შპს  ალიჯორჯია	<br />
				ს/კ: 402011568<br />
				ფაქტობრივი მისამართი<br />	
				ქ. თბილისი უ. ჩხეიძის 19<br />	
			</td>
			<td>
				თარიღი: <?php echo date('d/m/Y', strtotime($last['arrived_date'])); ?><br />
				ნომერი: <?php $inv->invNumb($last['userID'], $_GET['subroute']); ?><br />
			</td>
		</tr>
		<tr>
			<td>
				სს. "საქართველოს ბანკი"<br />		
				ანგ: № GE75BG0000000917631100<br />		
				ბანკის კოდი 220101502 / BAGAGE22 /<br />		
				დანიშნულება<br />		
				ტრანსპორტირების საფასური<br />		
			</td>
			<td>
                <img src="<?php echo ROOT_URL; ?>assets/img/logo_ali_color.png" width="200" />
			</td>
		</tr>
	</table>

	<h4>მიმღები</h4>

	<table class="table" style="width:100%">
		<tr>
			<td style="width:50%">
				სახელი, გვარი:				
			</td>
			<td>
				<?php echo $last['name']; ?>
			</td>
		</tr>
		<tr>
			<td>
				პირადი ნომერი			
			</td>
			<td>
				<?php echo $last['passport']; ?>
			</td>
		</tr>
	</table>

	<table class="table table-bordered">
		<tr class="active">
			<th>
				გზავნილის ნომერი	
			</th>
			<th>
				რაოდენობა
			</th>
			<th>
				წონა (კგ)
			</th>
			<th>
				ტარიფი	(USD)
			</th>
			<th>
				<div style="display:inline-block;">ეროვნული ბანკის კურსი</div>
			</th>
			<th>
				თანხა (ლარი)
			</th>
		</tr>
	<?php $totalPrice = 0; ?>
	<?php $totalWeight = 0; ?>

	<?php foreach($inv->selectProduct($last['userID'], $last['flightID']) as $item): ?>
		<tr>
			<td>
				<?php echo $item['productOrder']; ?>
			</td>
			<td>
				
			</td>
			<td>
				<?php echo $item['weight']; ?>
			</td>
			<td>
				<?php echo $item['tariff']; ?>
			</td>
			<td>
				<?php echo $currency = $item['currentUSDrate']; ?>
			</td>
			<td>
				<?php $itemPrice = round(($item['weight'] * $item['tariff']) * $currency, 2); echo number_format((float)$itemPrice, 2, '.', ''); ?>
			</td>
		</tr>
		<?php $totalPrice += $itemPrice; ?>
		<?php $totalWeight += $item['weight']; ?>
		<?php $count[] = $item['productOrder']; ?>
	<?php endforeach; ?>
		<tr>
			<td>
				<b>სულ</b>
			</td>
			<td>
				<?php echo count($count); unset($count);?>
			</td>
			<td>
				<?php echo $totalWeight; ?>
			</td>
			<td>
				
			</td>
			<td>
				
			</td>
			<td>
				<b><?php echo number_format((float)$totalPrice, 2, '.', ''); $_SESSION['pay']['totalPrice'] = $totalPrice; ?></b>
			</td>
		</tr>
	</table>

	<div>
	კონტაქტი:	<br />						
	ვებ გვერდი: www.AliGeorgia.Ge	<br />						
	ელ.ფოსტა:    info.aligeorgia@gmail.com		<br />					
	ტელეფონი:  (+032) 2 196 191		<br />					
								
	გმადლობთ, რომ სარგებლობთ ჩვენი მომსახურებით!			<br />				
	</div>
<?php
	// if($item['state'] == 'paid'){

		// <div class="invoice-paid" style="width:200px;position:relative;
	// border-radius:3px;
	// margin-top:-200px;
	// margin-bottom:200px;
	// text-align:center;
	// padding:20px;
	// background:#f0ad4e;
	// color:#fff;
	// border:1px solid #ccc;
	// text-shadow:1px 1px 1px #000;
	// -moz-transform: rotate(45deg);  /* Firefox */
	// -o-transform: rotate(45deg);  /* Opera */
	// -webkit-transform: rotate(45deg);  /* Safari y Chrome */
	// filter: progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand', M11=0.7071067811865476, M12=-0.7071067811865475, M21=0.7071067811865475, M22=0.7071067811865476); /* IE */">
			// გადახდილია !!!
		// </div>

	// }
?>
	<div style="page-break-after: always;"></div>
			
			<!---------------------------------------------------------------------------------------------------------->
			<!---------------------------------------------------------------------------------------------------------->
			<!---------------------------------------------------------------------------------------------------------->
			<!---------------------------------------------------------------------------------------------------------->
		<?php endforeach; ?>
	<?php endforeach; ?>
	</div>
</div>