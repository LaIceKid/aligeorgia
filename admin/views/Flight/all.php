<?php
if(!isset($_SESSION['admin_data'])){
	header('location:'.ROOT_URL);
}
$packages = new FlightModel();
$filight_text = $packages->flight_articles();
?>
<div class="rightBlock">
	<form action="" method="post" class="editFlight">
		<div class="form-group">
		  <label for="datepicker-start" class="datepicker-start">გაგზავნის ტარიღი</label>
		 <input type="text" id="datepicker-start" name="startDate" placeholder="გაგზავნის თარიღი" class="form-control"/>
		</div>
			<input type="submit" name="add" class="btn btn-success" value="რედაქტირება" />
	</form><br/>

	<h3>რეისები</h3>
	<table class="table table-striped">
<?php
		foreach($viewmodel as $item){
?>
			<tr>
				<th class="max">
					<a href="<?php echo ROOT_PATH; ?>flight/display/<?php echo $item['id']; ?>">რეისის № <?php Other::AxCode($item['id']); ?> <i>ფრენა: <?php echo $item['date']; ?> | ჩამოსვლა: <?php echo $item['endDate']; ?> | წონა: <?php echo $packages->sumWeight($item['id']); ?> | ამანათების რაოდენობა: <?php echo $packages->sumPackages($item['id']); ?></i>
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight/invoice/<?php echo $item['id']; ?>" class="btn btn-info">
						ინვოისი
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight/packageList/<?php echo $item['id']; ?>" class="btn btn-danger">
						ამანათების სია
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight/invoiceList/<?php echo $item['id']; ?>" class="btn btn-danger">
						ინვოისების სია
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight/edit/<?php echo $item['id']; ?>" class="btn btn-danger">
						რედაქტირება
					</a>
				</th>
				<th class="mini">
					<a href="<?php echo ROOT_PATH; ?>flight/delete/<?php echo $item['id']; ?>" class="btn btn-danger" onclick="return confirm('ნამდვილად გსურთ ამ რეისის წაშლა ?');">
						წაშლა
					</a>
				</th>
			</tr>
<?php
		}
?>
	</table>
</div>
