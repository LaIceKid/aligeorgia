<!doctype html>
<html lang="en">
<?php include_once ('includes/head.php'); ?>
<body class="sidebar-minified">
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <?php include_once ('includes/navbar.php'); ?>
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    <?php include_once ('includes/left_sidebar.php'); ?>

    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <?php include_once ('includes/header.php'); ?>
            <div class="container-fluid">
              <?php Messages::display(); ?>
              <?php require($view); ?>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
        <!-- RIGHT SIDEBAR -->
        <?php include_once ('includes/right_sidebar.php'); ?>
        <!-- END RIGHT SIDEBAR -->
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>
    <?php include_once ('includes/footer.php'); ?>
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<?php include_once ('includes/footer_js.php'); ?>
</body>
</html>
