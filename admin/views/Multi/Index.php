<div class="rightBlock">
	<h3>მულტი პროცესები</h3>
	<h5>მისაღებია მხოლოდ წინასწარ გამზადებული CSV ფაილი</h5>
	<form action="" method="post" enctype="multipart/form-data" class="multiForm multi-forms">
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <label class="btn btn-danger btn-file form-control" style="margin-bottom:10px;color:white;">
                        <i class="fa fa-file-o"></i> ფაილი <input type="file" name="file" style="display: none;">
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
		            <button type="submit" name="submit" class="btn btn-success form-control"><i class="fa fa-cogs"></i> დამუშავება</button>
                </div>
            </div>
        </div>
	</form>
	<br />
    <div class="table-responsive">
        <table class="table table-borderd" style="width:50%; display:inline-block;">
            <tr>
                <td>თრექინგ კოდი</td>
                <td>წონა</td>
                <td>AX კოდი</td>
                <td>სტატუსი</td>
                <td>რეისი</td>
                <td>ტარიფი</td>
                <td>მისვლის თარიღი</td>
                <td>გამოგზავნის თარიღი</td>
                <td>ჩამოსვლის თარიღი</td>
            </tr>
	</table>
    </div>
	<br /><hr /><br />
	<div style="width:50%; display:inline-block;">
		<?php
		$Multi = new MultiModel();
		// echo '<pre>';
		// print_r($Multi->csvArray());
		// echo '<pre>';
		$Multi->csvProcess($Multi->csvArray(), $Multi->prodArray());
		?>
	</div>
	<hr />
	<div class="invoiceDelete multi-forms">
        <h3>ინვოისების წაშლა</h3>
		<form action="" method="post" class="row">
            <div class="row">
                <div class="form-group">
                    <div class="col-md-6">
                        <input type="number" class="form-control" name="startPoint" placeholder="Start Point" />
                    </div>
                    <div class="col-md-6">
                        <input type="number" class="form-control" name="endPoint" placeholder="End Point" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <input type="submit" name="deleteInvoices" class="form-control btn btn-danger" value="წაშლა" />
                </div>
            </div>
		</form>
	</div>

	<br />

	<div class="invoiceReset multi-forms">
		<form action="" method="post" class="row">
			<h3>Alter Table (ინვოისები)</h3>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-6">
			            <input type="number" class="form-control" name="alterPoint" placeholder="Alter Point" />
                    </div>
                    <div class="col-md-6">
			            <input type="submit" name="alter" class="form-control btn btn-danger" value="Alter" />
                    </div>
                </div>
            </div>
		</form>
	</div>

	<hr /><hr /><hr />
	<div class="invoiceDelete multi-forms">
		<form action="" method="post" class="row">
			<h3>რეისის წაშლა</h3>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-6">
			            <input type="number" class="form-control" name="startPoint" placeholder="Start Point" />
                    </div>
                    <div class="col-md-6">
			            <input type="number" class="form-control" name="endPoint" placeholder="End Point" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <input type="submit" name="deleteFlights" class="form-control btn btn-warning" value="წაშლა" />
                </div>
            </div>

		</form>
	</div>

	<br />

	<div class="invoiceReset multi-forms">
		<form action="" method="post" class="row">
			<h3>Alter Table (რეისები)</h3>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-6">
		            	<input type="number" class="form-control" name="alterPoint" placeholder="Alter Point" />
                    </div>
                    <div class="col-md-6">
		            	<input type="submit" name="alterFlights" class="form-control btn btn-warning" value="Alter" />
                    </div>
                </div>
            </div>
		</form>
	</div>
</div>
