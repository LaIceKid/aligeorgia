<?php
class Route extends Model{
  public function router(){
    $route = array(
    	//MAINS
		'' => array('controller' => 'home', 'action' => 'index'),
		'home' => array('controller' => 'home', 'action' => 'index'),
		'login' => array('controller' => 'user', 'action' => 'login'),
		'language' => array('controller' => 'language', 'action' => 'select'),
		//PACKAGES
		'awaiting' => array('controller' => 'package', 'action' => 'awaiting'),
	    'warehouse' => array('controller' => 'package', 'action' => 'warehouse'),
	    'pending' => array('controller' => 'package', 'action' => 'pending'),
	    'arrived' => array('controller' => 'package', 'action' => 'arrived'),
		'obtained' => array('controller' => 'package', 'action' => 'obtained'),
	    'giveaway' => array('controller' => 'package', 'action' => 'giveaway'),
	    'sort-packages' => array('controller' => 'package', 'action' => 'sorting'),

	    'packagedata' => array('controller' => 'package', 'action' => 'packagedata'),
	    'packagedataflight' => array('controller' => 'package', 'action' => 'packagedataflight'),
	    'transfer-packages' => array('controller' => 'package', 'action' => 'transferPackages'),
	    'edit-package' => array('controller' => 'package', 'action' => 'edit'),
	    'update-package' => array('controller' => 'package', 'action' => 'update'),

	    'invoice' => array('controller' => 'package', 'action' => 'invoice'),
		//FLIGHTS
	    'flights' => array('controller' => 'flight', 'action' => 'Index'),
	    'flight-invoices' => array('controller' => 'flight', 'action' => 'invoice'),
	    'display-flight-packages' => array('controller' => 'flight', 'action' => 'display'),
	    'flight-package-list-clone' => array('controller' => 'flight', 'action' => 'packageList'),
	    'flight-invoice-list-clone' => array('controller' => 'flight', 'action' => 'invoiceList'),
	    'flight-edit' => array('controller' => 'flight', 'action' => 'edit'),
	    //MULTI
	    'multi-process' => array('controller' => 'multi', 'action' => 'Index'),
	    //USERS
	    'all-users' => array('controller' => 'users', 'action' => 'all'),
	    'copy-users' => array('controller' => 'users', 'action' => 'copy'),
	    'edit-user' => array('controller' => 'users', 'action' => 'edit'),
	    'update-user' => array('controller' => 'users', 'action' => 'update')
    );

    return $route;
  }

}
