
<script src="<?php echo ROOT_URL; ?>assets\vendor\jquery\jquery.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\bootstrap\js\bootstrap.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\pace\pace.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\jquery.easy-pie-chart\jquery.easypiechart.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\chartist\js\chartist.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\raphael\raphael.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\jquery-mapael\js\jquery.mapael.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\jquery-mapael\js\maps\world_countries.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\datatables\js-main\jquery.dataTables.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\datatables\js-bootstrap\dataTables.bootstrap.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\datatables-tabletools\js\dataTables.tableTools.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\chart-js\Chart.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\Flot\jquery.flot.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\Flot\jquery.flot.resize.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\vendor\jquery-slimscroll\jquery.slimscroll.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\scripts\klorofilpro-common.min.js"></script>
<script src="<?php echo ROOT_URL; ?>assets\scripts\main.js"></script>
