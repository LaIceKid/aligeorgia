<head>
    <title>AliGeorgia.ge Administration</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\vendor\bootstrap\css\bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\vendor\font-awesome\css\font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\vendor\themify-icons\css\themify-icons.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\vendor\pace\themes\orange\pace-theme-minimal.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\css\vendor\animate\animate.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\vendor\bootstrap-progressbar\css\bootstrap-progressbar-3.3.4.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\vendor\x-editable\bootstrap3-editable\css\bootstrap-editable.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\vendor\bootstrap-tour\css\bootstrap-tour.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\vendor\jqvmap\jqvmap.min.css">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\css\main.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\css\skins\sidebar-nav-darkgray.css" type="text/css">
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\css\skins\navbar3.css" type="text/css">
    <!-- FOR DEMO PURPOSES ONLY. You should/may remove this in your project -->
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\css\demo.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets\css\style.css?v=1.0.0">
    <!-- <link rel="stylesheet" href="<?php echo ROOT_URL; ?>demo-panel\style-switcher.css"> -->
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo ROOT_URL; ?>assets\img\favicon\apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo ROOT_URL; ?>assets\img\favicon\favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo ROOT_URL; ?>assets\img\favicon\favicon-16x16.png">
    <link rel="manifest" href="<?php echo ROOT_URL; ?>assets\img\favicon\site.webmanifest">
    <link rel="mask-icon" href="<?php echo ROOT_URL; ?>assets\img\favicon\safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
</head>
