<div id="sidebar-nav"
     class="sidebar">
    <nav>
        <ul class="nav" id="sidebar-nav-menu">
            <li>
                <a href="<?php echo ROOT_URL; ?>" class="active"><i class="ti-home"></i>
                    <span class="title">მთავარი</span></a>
            </li>
            <li class="panel">
                <a href="#dashboards"
                   data-toggle="collapse"
                   data-parent="#sidebar-nav-menu"><i
                            class="ti-package"></i>
                    <span class="title">ამანათები</span>
                    <i class="icon-submenu ti-angle-left"></i></a>
                <div id="dashboards"
                     class="in collapse">
                    <ul class="submenu">
                        <li>
                            <a href="<?php echo ROOT_URL; ?>awaiting/">მოლოდინის
                                რეჟიმი
                                <span class="label label-default"><?php echo $PackageModel->count_packages(''); ?></span></a>
                        </li>
                        <li>
                            <a href="<?php echo ROOT_URL; ?>warehouse/">მისულია
                                საწყობში
                                <span class="label label-info"><?php echo $PackageModel->count_packages('china-stock'); ?></span></a>
                        </li>
                        <li>
                            <a href="<?php echo ROOT_URL; ?>pending/">გამოგზავნილი
                                <span class="label label-warning"><?php echo $PackageModel->count_packages('sent'); ?></span></a>
                        </li>
                        <li>
                            <a href="<?php echo ROOT_URL; ?>arrived/">ჩამოსული
                                <span class="label label-primary"><?php echo $PackageModel->count_packages('arrived'); ?></span></a>
                        </li>
                        <li>
                            <a href="<?php echo ROOT_URL; ?>obtained/">გატანილი
                                <span class="label label-success"><?php echo $PackageModel->count_packages('obtained'); ?></span></a>
                        </li>
                        <li>
                            <a href="<?php echo ROOT_URL; ?>giveaway/">ამანათების
                                გაცემა</a>
                        </li>
                        <li>
                            <a href="<?php echo ROOT_URL; ?>sort-packages/">დახარისხება</a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="panel">
                <a href="#subPages"
                   data-toggle="collapse"
                   data-parent="#sidebar-nav-menu"
                   class="collapsed">
                    <i class="ti-files"></i>
                    <span class="title">გვერდები</span>
                    <i class="icon-submenu ti-angle-left"></i></a>
                <div id="subPages" class="collapse">
                    <ul class="submenu">
                        <li>
                            <a href="<?php echo ROOT_URL; ?>flights/">რეისები</a>
                        </li>
                        <li>
                            <a href="<?php echo ROOT_URL; ?>email/">ელ.
                                ფოსტა</a>
                        </li>
                        <li>
                            <a href="<?php echo ROOT_URL; ?>multi-process/">მულტი
                                პროცესები</a>
                        </li>
                        <li>
                            <a href="<?php echo ROOT_URL; ?>all-users/">მომხმარებლები</a>
                        </li>
                    </ul>
                </div>
            </li>

        </ul>
        <button type="button"
                class="btn-toggle-minified"
                title="Toggle Minified Menu">
            <i class="ti-arrows-horizontal"></i>
        </button>
    </nav>
</div>
