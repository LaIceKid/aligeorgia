<div class="content-heading clearfix">
    <div class="heading-left">
        <h1 class="page-title">
            <?php
                if(isset($lang[strtoupper($_GET['route'])])){
	                echo $lang[strtoupper($_GET['route'])];
                }else{
	                echo $lang['HOME'];
                }
            ?>
        </h1>
        <p class="page-subtitle">
            აღწერა გვერდისთვის "
	        <?php
                if(isset($lang[strtoupper($_GET['route'])])){
                    echo $lang[strtoupper($_GET['route'])];
                }else{
                    echo $lang['HOME'];
                }
	        ?>
            "
        </p>
    </div>
    <ul class="breadcrumb">
        <li>
            <a href="<?php echo ROOT_URL; ?>"><i class="fa fa-home"></i>
                <?php echo $lang['HOME']; ?>
            </a>
                <?php
                    if(isset($lang[strtoupper($_GET['route'])])){
                        echo ' <span class="ti-angle-right" style="font-size:10px;"></span> ';
                        echo $lang[strtoupper($_GET['route'])];
                    }
                ?>

        </li>
        <!-- <li><a href="#">Dashboards</a></li>
        <li class="active">Dashboard v3</li> -->
    </ul>
</div>
